
# Run Locally


### Start Mongodb
$ mongod --dbpath /usr/local/var/mongodb/

### Start App
$ gulp

### Compile App
$ gulp build

### Deploy App
$ cap production deploy

- - -

# Export

1. Load All Lists
2. Load All Categories
3. Check Categories
4. Create missing Categories
5. Remap Categories
6. Inject all Lists


## Import  Test Content
``
[{"_id":"55355fd2589ddd0000583824","user":{"_id":"55355ee47b3cfb3be908acd8","displayName":"test test"},"category":{"_id":"55355f2c7b3cfb3be908acd9","marker":4,"name":"Brains"},"__v":0,"items":[{"title":"awfawfwfwafawfawfawfwaa","content":[{"content":"fwa","type":false},{"content":"f","type":false},{"content":"wfwafawfawfawfwaa","type":false}],"divider":false},{"title":"wfwafawfawfawfwaa","content":[{"type":"","content":"wfwafawfawfawfwaa"}],"divider":false}],"updated":"2015-04-20T22:03:21.971Z","created":"2015-04-20T20:21:38.971Z","count":2,"itemIndex":1,"color":0,"mode":0,"locked":0,"block":false,"favorite":false,"share":false,"description":"","name":"Brainy Brainlist (Duplicate)"},{"_id":"55355f2e7b3cfb3be908acda","user":{"_id":"55355ee47b3cfb3be908acd8","displayName":"test test"},"category":{"_id":"55355f2c7b3cfb3be908acd9","marker":4,"name":"Brains"},"__v":2,"items":[{"title":"awfawfwfwafawfawfawfwaa","content":[{"content":"fwa","type":false},{"content":"f","type":false},{"content":"wfwafawfawfawfwaa","type":false}],"divider":false},{"title":"wfwafawfawfawfwaa","content":[{"type":"","content":"wfwafawfawfawfwaa"}],"divider":false}],"updated":"2015-04-20T20:21:38.990Z","created":"2015-04-20T20:18:54.294Z","count":2,"itemIndex":1,"color":0,"mode":0,"locked":0,"block":false,"favorite":false,"share":false,"description":"","name":"Brainy Brain Superlist"}]
``
----------------------------------------------

# CONCEPT
- List Attributes
	- Public/Shareable
	-  Favorites
- List Types
	- Default (edit)
	- Vocbulary
	- Image List
	- QUIZ?

- Item Types
	- Image
	- Text/Vocab
	- Code
	- Gist
	- Link
- Share
	- 	Export
	- 	Markdown
	-


----------------------------------------------


# DEBUGGING

## Access Scope from Console
```
$("[data-ng-controller=ListsController]").scope().isLastSubItem(1,1);
``


# MEAN JS

## ADD MODULES
### REGISTER MODULES
### INCLUDE JS/ETC
