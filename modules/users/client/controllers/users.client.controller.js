'use strict';
/*global Blob:false*/
/*global saveAs:false*/
/*global $:false */

angular.module('users').controller('UsersController', ['$scope','$rootScope', '$http', '$location', 'Users', 'Authentication','$modal',
	function($scope,$rootScope, $http, $location, Users, Authentication, $modal) {
		$scope.user = Authentication.user;
		var _$scope = $scope;


	       $scope.showUsersModal = function (user) {
             console.log('showUsersModal',user);
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/users/views/user-settings-modal.client.view.html',
                  controller: function ($scope, $modalInstance) {
										$scope.exportLists = function () {
											_$scope.showExportModal();
										};
										$scope.importLists = function () {
											_$scope.showImportModal();
										};
                      $scope.ok = function () {
                        $modalInstance.close();
                      };
                      $scope.cancel = function () {
                        $modalInstance.close();
                      };
                    }
                }).result;
            };


						$scope.showExportModal = function () {
	             return $modal.open({
	                size: 'md',
	                  templateUrl: 'modules/users/views/export-lists-modal.client.view.html',
	                  controller: function ($scope, $rootScope, $modalInstance) {
												$scope.allLists  = '';

												$rootScope.getAllLists(function(lists){
													$scope.allLists = JSON.stringify(lists);
												});
												$scope.download = function () {
													var blob = new Blob([$scope.allLists], {type: 'text/plain;charset=utf-8'});
													window.saveAs(blob, 'monolie-export.json');

												};
	                      $scope.ok = function () {
	                        $modalInstance.close();
	                      };
	                      $scope.cancel = function () {
	                        $modalInstance.close();
	                      };
	                    }
	                }).result;
	            };


							$scope.showImportModal = function () {
								return $modal.open({
										size: 'md',
											templateUrl: 'modules/users/views/import-lists-modal.client.view.html',
											controller: function ($scope, $rootScope, $modalInstance) {
													$scope.listsString  = '';
													$scope.error = '';
													$scope.file = false;
													$scope.triggerUpload = function () {
														$('#uploadImportFileInput').click();
													};
													$scope.upload = function () {
														var fileInput = document.getElementById('uploadImportFileInput');
															var file = fileInput.files[0];
															// var textType = /text.*/;
															var textType = 'application/json';


															if (file.type.match(textType)) {
																var reader = new FileReader();

																reader.onload = function(e) {
																	console.log('reader.result',reader.result);
																	$scope.listsString = reader.result;
																	$scope.$apply();
																};

																	reader.readAsText(file);
															} else {
																$scope.error = 'File not supported!';
															}
													};
													$scope.import = function () {
														try{
															$rootScope.createLists(JSON.parse($scope.listsString));
															$scope.ok();
														}catch(e){
																$scope.error = 'Not valid, dude. Better check that syntax.';
														}
														// $rootScope.createLists(JSON.parse($scope.listsString));
													};
													$scope.ok = function () {
														$modalInstance.close();
													};
													$scope.cancel = function () {
														$modalInstance.close();
													};
												}
										}).result;
								};

	}
]);
