'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope','$rootScope', '$http', '$location', 'Users', 'Authentication',
	function($scope,$rootScope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/api/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
				$rootScope.$broadcast('success',{id:'userPassword',message:response.message});
			}).error(function(response) {
				$rootScope.$broadcast('error',{id:'userPassword',message:response.message});
				$scope.error = response.message;
			});
		};
	}
]);
