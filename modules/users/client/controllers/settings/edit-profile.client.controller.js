'use strict';

angular.module('users').controller('EditProfileController', ['$scope', '$rootScope','$http', '$location', 'Users', 'Authentication',
	function($scope,$rootScope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
					$rootScope.$broadcast('success',{id:'#userProfile',message:response.message});
				}, function(response) {
					$rootScope.$broadcast('error',{id:'#userProfile',message:response.message});
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};
	}
]);
