'use strict';

//Setting up route
angular.module('lists').config(['$stateProvider',
	function($stateProvider) {
		// Lists state routing
		$stateProvider.
		// this should be equivalent to the deiail vie
		state('listsCreate', {
			url: '/lists/create',
			templateUrl: 'modules/lists/views/create-list.client.view.html'
		}).

		state('listsView', {
			url: '/lists/:listId',
			templateUrl: 'modules/lists/views/view-list.client.view.html'
		}).

		// does this make sense?
		state('listsRecent', {
			url: '/recent/:listId',
			templateUrl: 'modules/lists/views/list-lists-recent.client.view.html'
		}).
		// state('listsFavoritesDetail', {
		// 	url: '/favorites/:listId',
		// 	templateUrl: 'modules/lists/views/view-list.client.view.html'
		// }).
		state('listListsByCategoryDetail', {
			url: '/lists/categories/:catId/:listId',
			templateUrl: 'modules/lists/views/list-lists-categories.client.view.html'
		}).


		state('home', {
			url: '/:listId',
			templateUrl: 'modules/lists/views/list-lists-recent.client.view.html'
		}).
		state('listsCategorized', {
			url: '/all/:listId',
			templateUrl: 'modules/lists/views/list-lists-categorized.client.view.html'
		}).


		// state('listRecent', {
		// 	url: '/recent',
		// 	templateUrl: 'modules/lists/views/list-lists-recent.client.view.html'
		// }).
		state('listFavorites', {
			url: '/favorites/:listId',
			templateUrl: 'modules/lists/views/list-lists-favorites.client.view.html'
		}).
		state('listPublic', {
			url: '/public',
			templateUrl: 'modules/lists/views/list-lists-public.client.view.html'
		}).
		state('listListsByCategory', {
			url: '/lists/categories/:catId',
			templateUrl: 'modules/lists/views/list-lists-categories.client.view.html'
		});

		// state('editList', {
		// 	url: '/lists/:listId/edit',
		// 	templateUrl: 'modules/lists/views/edit-list.client.view.html'
		// });
	}
]);
