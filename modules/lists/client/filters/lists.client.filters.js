'use strict';

angular.module('lists').filter('addHttp', function() {
  return function(item) {
  var prefix = 'http';
    if (item.substr(0, prefix.length) !== prefix)
    {
        item = 'http://' + item;
    }
    return item;
  };
});
