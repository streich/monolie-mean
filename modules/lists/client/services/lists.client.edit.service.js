'use strict';
/*global $:false */
/*global _:false */
/* jscs: disable */

//modal Service
angular.module('lists').service('listsEditService', [ '$rootScope','Lists','ListsFavorites', 'ListsPublic', 'ListsRecent', 'ListsByCategory','ListsCollection',
    function ( $rootScope, Lists, ListsFavorites, ListsPublic, ListsRecent, ListsByCategory, ListsCollection) {
    	this.list = {};



      // ort import?
      $rootScope.createLists = function(lists) {
          $rootScope.listItemsLoaded = false;
          var categorymap = {};

          function _remapCategory(category){
            var returnValue = false;
            $.each($rootScope.categories,function(){
              // console.log((this.name === category.name));
              if(!_.isNull(this) && !_.isNull(category) && this.name === category.name){
                // console.log(this._id, this.name);
                returnValue = this._id;
                return this._id;
               }
            });
            return returnValue;
          }
          function cleanLists(lists){
            var cleanedLists = [];
            angular.forEach(lists,function(item){
              delete item.__v;
              delete item._id;
              delete item.user;
              item.category = _remapCategory(item.category);
              cleanedLists.push(item);
            });
            return cleanedLists;
          }
          // Create new List object
          var collection = new ListsCollection({lists:cleanLists(lists)});
          console.log(collection);
          // Redirect after save
          collection.$save(function(response) {
              return false;
          }, function(errorResponse) {
            console.log(errorResponse.data.message);
              // $scope.error = errorResponse.data.message;
          });
      };


      $rootScope.getAllLists = function(callback){
            var lists =  Lists.query( function(response) {
              var lists = [];
              angular.forEach(response,function(item){
                lists.push(item);
              });
              console.log('getAllLists',lists);
              callback(lists);
            });
        };


  }]);
