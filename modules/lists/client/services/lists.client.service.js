'use strict';

//Lists service used to communicate Lists REST endpoints
angular.module('lists').factory('Lists', ['$resource',
	function($resource) {
		return $resource('api/lists/:listId', { listId: '@_id'
		}, {
			get:    {
				method:'GET'
				// cache: true
			},
			update: {
				method: 'PUT'
				// cache: true
			}
		});
	}
]);

angular.module('lists').factory('ListsCollection', ['$resource',
	function($resource) {
		return $resource('api/collection', {
		}, {
			get:    {
				method:'GET'
				// cache: true
			},
			put: {
				method: 'PUT'
			}
		});
	}
]);

angular.module('lists').factory('ListsFavorites', ['$resource',
	function($resource) {
		return $resource('api/favorites', {
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);


angular.module('lists').factory('ListsRecent', ['$resource',
	function($resource) {
		return $resource('api/recent', {
		}, {
			update: {
				method: 'PUT'
			},
			query: {
			 method: 'GET',
			  isArray:true,
			//  responseType: 'json',
			 transformResponse: function(data,headers) {
				 var items  = angular.fromJson(data);
				 angular.forEach(items,function(item,key){
					 if(item.description) item.height = 55;
					 else item.height = 40;
				 });
				 return items;
			 }
	 		}
		});
	}
]);


angular.module('lists').factory('ListsByCategory', ['$resource',
	function($resource) {
		return $resource('api/lists/categories/:catId', { catId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			query: {
			 method: 'GET',
				isArray:true,
			//  responseType: 'json',
			 transformResponse: function(data,headers) {
				 var items  = angular.fromJson(data);
				 angular.forEach(items,function(item,key){
					 if(item.description) item.height = 55;
				 });
				 return items;
			 }
			},
			remove: {
				method: 'DELETE',
				params: {listId: '@_id'},
                 url: 'api/lists/:listId'

			}
		});
	}
]);



angular.module('lists').factory('ListsPublic', ['$resource',
	function($resource) {
		return $resource('api/lists/public', {
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
