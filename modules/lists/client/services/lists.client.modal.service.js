'use strict';

//modal Service
angular.module('lists').service('modalService', ['$modal','$timeout','utilityService',
    function ($modal,$timeout,utilityService) {


       this.showModalListOptions = function (list,listScope) {
             console.log('showModalListOptions',list);
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/lists/views/modal-list-settings.html',
                  controller: function ($scope, $modalInstance) {
                      console.log('showModalListOptions Controller',list);
                      $scope.list = list;
                      $scope.$listScope =  listScope;

                       $scope.duplicateList = function(){
                          listScope.duplicateList(list);
                      };
                      $scope.changeCategory = function(){
                         listScope.modals.openCategoriesModal(list);
                     };
                      $scope.exportAsMarkdown = function(){
                          listScope.modals.openExportModal(list);
                      };
                      $scope.importFromMarkdown = function(){
                          listScope.modals.openImportModal(list);
                      };
                       $scope.removeList = function(){
                          listScope.remove(list);
                           $modalInstance.close();
                      };
                      $scope.ok = function () {
                        $modalInstance.close($scope.list);
                      };
                      $scope.cancel = function () {
                        $modalInstance.close($scope.list);
                      };
                    }
                }).result;
            };


   this.showModalListModes = function (list,listScope) {
         console.log('showModalListOptions',list);
         return $modal.open({
            size: 'md',
              templateUrl: 'modules/lists/views/modal-list-modes.html',
              controller: function ($scope, $modalInstance) {
                  console.log('showModalListOptions Controller',list);
                  $scope.list = list;
                  $scope.$listScope =  listScope;


                  $scope.ok = function () {
                    $modalInstance.close($scope.list);
                  };
                  $scope.cancel = function () {
                    $modalInstance.close($scope.list);
                  };
                }
            }).result;
        };

       this.showModalListExport = function (list,listScope) {
             console.log('showModalListOptions',list);
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/lists/views/modal-list-export.html',
                  controller: function ($scope, $modalInstance) {
                      $scope.markdown = '';
                      $timeout(function(){
                           $scope.markdown = utilityService.convertListToMarkdown(list);
                      },10);
                      $scope.ok = function () {
                        $modalInstance.close(list);
                      };
                      $scope.cancel = function () {
                        $modalInstance.close(list);
                      };
                    }
                }).result;
            };

         this.showModalListImport = function (list,listScope) {
             console.log('showModalListOptions',list);
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/lists/views/modal-list-import.html',
                  controller: function ($scope, $modalInstance) {
                      $scope.markdown = '';
                      $scope.html = '';
                      $timeout(function(){
                           $scope.markdown = utilityService.convertListToMarkdown(list);
                      },10);

                      $scope.convert = function () {
                        $modalInstance.close(utilityService.convertMarkdownToList($scope.markdown));

                      };
                      $scope.ok = function () {
                        $modalInstance.close();
                      };
                      $scope.cancel = function () {
                        $modalInstance.close();
                      };
                    }
                }).result;
            };


       this.showModalListCategories = function (list,categories,listScope) {
             console.log('showModalListOptions',list);
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/lists/views/modal-list-categories.html',
                  controller: function ($scope, $modalInstance) {
                     $scope.list =  list;
                     $scope.catId = false;
                     $scope.listScope =  listScope;
                     $scope.categories =  categories;
                      $scope.chooseCategory = function (catId) {
                        console.log('chooseCategory',catId);
                        $modalInstance.close(catId);
                      };

                    $scope.ok = function () {
                        $modalInstance.close();
                      };
                      $scope.cancel = function () {
                        $modalInstance.close();
                      };
                    }
                }).result;
            };

          this.showModalListItem = function (listScope,index) {
             return $modal.open({
                   size: 'md',
                  templateUrl: 'modules/lists/views/modal-item-options.html',
                  controller: function ($scope, $modalInstance,$filter) {
                     $scope.$listScope =  listScope;
                     $scope.duplicateListItem = function(){
                          listScope.duplicateItem(index);
                      };
                      $scope.addDefaultTag = function(){
                          listScope.addTagToItem(index,'New Tag');
                      };
                       $scope.addRatingTag = function(){
                          listScope.addTagToItem(index,'1.1','number');
                      };
                         $scope.addDateTag = function(){
                        listScope.addTagToItem(index,$filter('date')(new Date(), 'MM/dd/yyyy'),'date');
                      };
                      $scope.removeItem = function () {
                          listScope.removeItem(index);
                      };

                      $scope.ok = function () {
                        $modalInstance.close();
                      };

                    }
                }).result;
            };

          this.showModalTagDefault = function (listScope,itemIndex,tagIndex) {
             return $modal.open({
                  size: 'md',
                  templateUrl: 'modules/lists/views/modal-tag-default.html',
                  controller: function ($scope, $modalInstance) {
                    $scope.tag =  listScope.getTagFromItem(itemIndex,tagIndex);

                    $scope.removeTag = function () {
                    $scope.tag =  listScope.removeTagFromItem(itemIndex,tagIndex);
                      $modalInstance.close();
                    };
                    $scope.ok = function () {
                      $modalInstance.close($scope.tag);
                    };

                    }
                }).result;
            };

          // this.showModalTag = function (listScope,itemIndex,tagIndex,type) {
          //   if(!type) type = 'default';
          //    return $modal.open({
          //         size: 'md',
          //         templateUrl: 'modules/lists/views/modal-tag-'+type+'.html',
          //         controller: function ($scope, $modalInstance) {
          //           $scope.tag =  listScope.getTagFromItem(itemIndex,tagIndex);

          //           $scope.removeTag = function () {
          //             $scope.tag =  listScope.removeTagFromItem(itemIndex,tagIndex);
          //             $modalInstance.close();
          //           };
          //           $scope.ok = function () {
          //             $modalInstance.close($scope.tag);
          //           };

          //           }
          //       }).result;
          //   };

            this.showModalTagNumber = function (listScope,itemIndex,tagIndex) {
               return $modal.open({
                  size: 'md',
                  templateUrl: 'modules/lists/views/modal-tag-number.html',
                  controller: function ($scope, $modalInstance) {
                    $scope.tag =  listScope.getTagFromItem(itemIndex,tagIndex);
                    $scope.numbers = [];

                    for(var i = 0; i <= 100; i+=1){
                      $scope.numbers.push((i/10).toFixed(1));
                    }

                    $scope.removeTag = function () {
                      $scope.tag =  listScope.removeTagFromItem(itemIndex,tagIndex);
                      $modalInstance.close();
                    };
                    $scope.ok = function () {
                      $modalInstance.close($scope.tag);
                    };
                  }

                }).result;
            };

            this.showModalTagDate = function (listScope,itemIndex,tagIndex) {
               return $modal.open({
                  size: 'md',
                  templateUrl: 'modules/lists/views/modal-tag-date.html',
                  controller: function ($scope, $modalInstance, $filter) {
                    $scope.tag =  listScope.getTagFromItem(itemIndex,tagIndex);


                $scope.$watch('dt', function(newModel,oldModel){
                      console.log(newModel);
                      $scope.tag.content = $filter('date')(newModel, 'MM/dd/yyyy');
                }, true);

                   $scope.today = function() {
                      $scope.dt = new Date();
                    };


                    $scope.dt = new Date($scope.tag.content);

                    $scope.clear = function () {
                      $scope.dt = null;
                    };

                    // Disable weekend selection
                    $scope.disabled = function(date, mode) {
                      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                    };

                    // $scope.toggleMin = function() {
                    //   $scope.minDate = $scope.minDate ? null : new Date();
                    // };
                    // $scope.toggleMin();

                    // $scope.open = function($event) {
                    //   $event.preventDefault();
                    //   $event.stopPropagation();

                    //   $scope.opened = true;
                    // };
                    //





                    $scope.dateOptions = {
                      formatYear: 'yy',
                      startingDay: 1
                    };

                    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];

                    $scope.removeTag = function () {
                      $scope.tag =  listScope.removeTagFromItem(itemIndex,tagIndex);
                      $modalInstance.close();
                    };
                    $scope.ok = function () {
                      $modalInstance.close($scope.tag);
                    };
                  }

                }).result;
            };


    }]);
