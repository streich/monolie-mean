'use strict';
/*global $:false */
/*global _:false */
/*global marked:false */
/*global rangy:false */
/* jscs: disable */



//autoSaveService Service
angular.module('lists').service('autoSaveService', ['$log','$rootScope',
    function($log,$rootScope) {

        function callback(){}

        function _callCallback(){
          if(changesMade){
            callback();
            changesMade = false;
          }
        }
        var _callbackDebounced = _.debounce(_callCallback, 15000);
        var changesMade = false;

        $rootScope.$on('leaveList',function(){
          $log.log('autosave');
          _callCallback();
        });

        this.setCallback = function(newCallback){
          callback = newCallback;
          _callbackDebounced = _.debounce(_callCallback, 15000);
        };

        this.trigger = function(){
          changesMade = true;
          _callCallback();
        };

        this.triggerDebounced = function(){
          changesMade = true;
          _callbackDebounced();
        };


    }
]);

//utility Service
angular.module('lists').service('messageService', [
    function() {

        this.infoMessage = function(message){
        };

        this.errorMessage = function(message){

        };

        this.successMessage = function(message){

        };

    }
]);

//utility Service
angular.module('lists').service('globalsService', [
    function() {
        this.blockParser = false;

    }
]);

//utility Service
angular.module('lists').service('utilityService', ['$modal',
    function($modal) {


        this.validation = {};

        this.openUrlInNewWindow = function(url){
            var win = window.open(url, '_blank');
            win.focus();
        };

        this.isDateString = function(value) {
            return (value.length === 10 && new Date(Date.parse(value)) !== 'Invalid Date');
        };

        this.isRatingString = function(value) {
            // console.log(Number(value));
            if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value) && Number(value) < 10.1){
                return Number(value);
            }
            return false;
        };

        this.isNumberString = function(value) {
            if (/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/
                .test(value))
                return Number(value);
            return false;
        };


        this.isUrlString = function(str) {
          var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
          if(!pattern.test(str)) {
            return false;
          } else {
            return true;
          }
        };


        this.isQuoteString = function(str) {
            var firstChar = str.substr(0,1);
            return (firstChar === '"'  || firstChar === '\'' );
            // return (/\".+\"|\'.+\'/.test(value));
        };

         this.isCodeString = function(str) {
            var firstChars = str.substr(0,6);
            return (firstChars === '//code' );
            // return (/\".+\"|\'.+\'/.test(value));
        };
        this.isDivider = function(string){
                return (string.substr(0,1) === '#' || string.substr(string.length-1,1) === ':' || string.substr(string.length-2,2) === ': ');
        };

        this.isTimeString = function(value) {
            return /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(value);
        };


        this.isPriceString = function(value) {
            return /^([0-1]?[0-9]|2[0-4]).([0-5][0-9])(:[0-5][0-9])?$/.test(value);
        };


        this.getTitleInputByIndex = function(index) {
            return $('[data-item-index="'+index+'"]')[0];
        };


        this.getContentInputByIndex = function(titleIndex,contentIndex) {
            return $('[data-item-index="'+titleIndex+'"][data-sub-index="'+contentIndex+'"]')[0];
        };


        this.getIndexFromElement = function(element) {
          if ($(element).hasClass('input-title')) {
            return this.getItemIndex(element);
          } else if ($(element).hasClass('input-content')) {
            return this.getSubItemIndex(element);
          } else{
            return this.getItemIndex($(element).find('.input-title'));
          }
        };

        this.getIndexes = function(element) {

        };
        this.getSubItemIndex = function(element) {
            return parseInt($(element).attr('data-sub-index'));
        };
        this.getItemIndex = function(element) {
            return parseInt($(element).attr('data-item-index'));
        };


        this.getInputValue = function(input) {
            var string = $(input).val();
            if(!string || string === '') string = $(input).text();
            return string;
        };

        this.setInputValue = function(input,value,append,setCaretAtEnd) {
          var caretPosition = 0;
            if(input.tagName === 'TEXTAREA'){
              caretPosition = $(input).val().length;
              if(append) value = $(input).val() + value;
              $(input).val(value);
            }else{
              caretPosition = $(input).text().length;

              if(append) value = $(input).text() + value;
              $(input).html(value);
            }

            function setCaret() {
              // console.log('setCaret',caretPosition);
                var range = document.createRange();
                var sel = window.getSelection();
                range.setStart(input.childNodes[0], caretPosition);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
                input.focus();
            }
            if(setCaretAtEnd) setCaret();

            $(input).trigger('input');
            return value;
        };

        this.isCaretAtEnd = function(input) {
            // if($(input).hasClass('link')) return true;
            //debug like this: setTimeout(function(){console.log(_getSelectionTextInfo($(':focus')[0]));},3000);
            function _getSelectionTextInfo(el) {
                var atStart = false,
                    atEnd = false;
                var selRange, testRange;
                if (window.getSelection) {
                    var sel = window.getSelection();
                    if (sel.rangeCount) {
                        selRange = sel.getRangeAt(0);
                        testRange = selRange.cloneRange();

                        testRange.selectNodeContents(el);
                        testRange.setEnd(selRange.startContainer, selRange.startOffset);
                        atStart = (testRange.toString() === '');

                        testRange.selectNodeContents(el);
                        testRange.setStart(selRange.endContainer, selRange.endOffset);
                        atEnd = (testRange.toString() === '');
                    }
                } else if (document.selection && document.selection.type !== 'Control') {
                    selRange = document.selection.createRange();
                    testRange = selRange.duplicate();

                    testRange.moveToElementText(el);
                    testRange.setEndPoint('EndToStart', selRange);
                    atStart = (testRange.text === '');

                    testRange.moveToElementText(el);
                    testRange.setEndPoint('StartToEnd', selRange);
                    atEnd = (testRange.text === '');
                }

                return {
                    atStart: atStart,
                    atEnd: atEnd
                };
            }
            // var value = input.value;
            // if (input.value) {
            //     // console.log('isCaretAtEnd (inp<!--  -->ut)',(input.value.length === input.selectionEnd));
            //     return (input.value.length === input.selectionEnd);
            // } else {
                // console.log('isCaretAtEnd',_getSelectionTextInfo(input).atEnd);
                return _getSelectionTextInfo(input).atEnd;
            // }
        };

        this.isCaretAtStart = function(element) {
            var position = 0;


            function _isSelectionEmpty(){
                return window.getSelection().toString() === '';
            }

            // this works with contenteditables as well
            function _getCaretCharacterOffsetWithin(element) {
                var caretOffset = 0;
                var doc = element.ownerDocument || element.document;
                var win = doc.defaultView || doc.parentWindow;
                var sel;
                if (typeof win.getSelection !== 'undefined') {
                    sel = win.getSelection();
                    if (sel.rangeCount > 0) {
                        var range = win.getSelection().getRangeAt(0);
                        var preCaretRange = range.cloneRange();
                        preCaretRange.selectNodeContents(element);
                        preCaretRange.setEnd(range.endContainer, range.endOffset);
                        caretOffset = preCaretRange.toString().length;
                    }
                } else if ((sel = doc.selection) && sel.type !== 'Control') {
                    var textRange = sel.createRange();
                    var preCaretTextRange = doc.body.createTextRange();
                    preCaretTextRange.moveToElementText(element);
                    preCaretTextRange.setEndPoint('EndToEnd', textRange);
                    caretOffset = preCaretTextRange.text.length;
                }
                return caretOffset;
            }

            if(!_isSelectionEmpty())
                return false;

            if (element.tagName === 'TEXTAREA') {
                position = element.selectionStart;
            } else {
                position = _getCaretCharacterOffsetWithin(element);
            }

            // console.log('isCaretAtStart',(position === 0 ));
            return (position === 0);

        };

        this.isTextSelected = function(input) {
            if (typeof input.selectionStart === 'number') {
                return input.selectionStart === 0 && input.selectionEnd === input.value.length;
            } else if (typeof document.selection !== 'undefined') {
                this.setFocusTo(input);
                return document.selection.createRange().text === input.value;
            }
        };

        this.setFocusTo = function(element, callback, moveCaretToStart) {
            if (element.tagName !== 'TEXTAREA') {
                element.onfocus = function() {
                    function select(){
                      var sel, range;
                      if (window.getSelection && document.createRange) {
                          range = document.createRange();
                          range.selectNodeContents(element);
                          range.collapse(moveCaretToStart);
                          sel = window.getSelection();
                          sel.removeAllRanges();
                          sel.addRange(range);
                      } else if (document.body.createTextRange) {
                          range = document.body.createTextRange();
                          range.moveToElementText(element);
                          range.collapse(moveCaretToStart);
                          range.select();
                      }
                    }
                    select();
                    window.setTimeout(select,1); //@TODO check if this is necessary
                };
            }
            // window.setTimeout(function() {
            element.focus();
            element.onfocus = null;

            if (callback) callback();
            $('.column-detail .column-inner')[0].scrollLeft = 0; // slideshow scroll bug workaround
            // }, 10  );
        };

        //@see http://stackoverflow.com/questions/5740640/contenteditable-extract-text-from-caret-to-end-of-element
        this.extractContentAfterCaret = function(blockEl){
            var sel = window.getSelection();
            if (sel.rangeCount) {
                var selRange = sel.getRangeAt(0);
                if (blockEl) {
                    var range = selRange.cloneRange();
                    range.selectNodeContents(blockEl);
                    range.setStart(selRange.endContainer, selRange.endOffset);
                    var extractedContents  =  $('<div>').append(range.extractContents()).html();
                    $(blockEl).trigger('input');
                    return extractedContents;
                }
            }
            return '';
        };

        this.moveCaretToEnd = function(el){
          el.focus();
           if (typeof window.getSelection !== 'undefined' && typeof document.createRange !== 'undefined') {
               var range = document.createRange();
               range.selectNodeContents(el);
               range.collapse(false);
               var sel = window.getSelection();
               sel.removeAllRanges();
               sel.addRange(range);
           } else if (typeof document.body.createTextRange !== 'undefined') {
               var textRange = document.body.createTextRange();
               textRange.moveToElementText(el);
               textRange.collapse(false);
               textRange.select();
           }
        };

        this.moveCaretToStart = function(el){
          el.focus();
           if (typeof window.getSelection !== 'undefined' && typeof document.createRange !== 'undefined') {
               var range = document.createRange();
               range.selectNodeContents(el);
               range.collapse(true);
               var sel = window.getSelection();
               sel.removeAllRanges();
               sel.addRange(range);
           } else if (typeof document.body.createTextRange !== 'undefined') {
               var textRange = document.body.createTextRange();
               textRange.moveToElementText(el);
               textRange.collapse(true);
               textRange.select();
           }
        };

        this.convertListToMarkdown = function(list){
            function _addMarkdownItem(text,prefix,suffix){
               if(prefix) listAsMarkdown = listAsMarkdown + prefix;
                listAsMarkdown = listAsMarkdown + text;
               if(suffix) listAsMarkdown = listAsMarkdown + suffix;
               listAsMarkdown = listAsMarkdown + '\n\n';

            }

            var listAsMarkdown = '';
            _addMarkdownItem(list.name,'# ');
            if(list.description) _addMarkdownItem(list.description,'***','***');
            _addMarkdownItem('- - -');
            $.each(list.items,function(){
                if(!this.divider){

                    _addMarkdownItem(this.title,'### ');
                    $.each(this.content,function(){
                        if(this.content !== 'undefined' && this.content !== ''){
                            switch(this.type){
                                case 'link': _addMarkdownItem(this.content,'['+this.content+'](',')'); break;
                                case 'code': _addMarkdownItem(this.content,'```\n','```'); break;
                                case 'quote': _addMarkdownItem(this.content,'> '); break;
                                case 'divider': _addMarkdownItem(this.content,'#### '); break;
                                default: _addMarkdownItem(this.content); break;
                            }
                        }
                    });
                  }else{
                    _addMarkdownItem(this.title,'## ');

                  }
           });

            return listAsMarkdown;
        };

        this.convertListToHtml = function(list){
            var listAsMarkdown = this.listAsMarkdown(list);
            var listAsHtml = this.convertMarkdownToHtml(listAsMarkdown);
            return listAsHtml;
        };

        this.convertMarkdownToHtml = function(listAsMarkdown){
            marked.setOptions({
              renderer: new marked.Renderer(),
              gfm: true,
              tables: false,
              breaks: false,
              pedantic: false,
              sanitize: true,
              smartLists: true,
              smartypants: false
            });

            var html = marked(listAsMarkdown);
            return html;

        };

        this.convertMarkdownToList = function(listAsMarkdown){
            var listAsHtml = this.convertMarkdownToHtml(listAsMarkdown);
            var list = {};

            var items = [];
            var allowAdding = false; // skip first elements in list
            var itemIndex = -1;

            function _addItem(item,divider){
                if(!allowAdding) return;
                // item = $(item);
                items.push({title:item.innerHTML, divider: divider, content:[]});
                itemIndex++;
            }

             function _addSubItem(item,type){
                if(!allowAdding) return;
                item = $(item);
                if(item.children().length > 0 && item.children()[0].tagName === 'A') type = 'link';
                items[itemIndex].content.push({content:item.text(), type: type});
            }
            $.each($(listAsHtml),function(){
                switch(this.tagName){
                    case 'H1': _addItem(this,true); break;
                    case 'H2': allowAdding= true; _addItem(this,true);  break;
                    case 'H3': allowAdding= true;  _addItem(this,false); break;
                    case 'P': _addSubItem(this); break;
                    case 'PRE': _addSubItem(this,'code'); break;
                    case 'CODE': _addSubItem(this,'code'); break;
                    case 'UL': _addSubItem(this); break;
                    case 'A': _addSubItem(this,'link'); break;
                    case 'BLOCKQUOTE': _addSubItem(this,'quote'); break;
                    case 'H4': _addSubItem(this,'divider'); break;
                    default: break;
                }
            });
            return items;
        };


    }
]);
