'use strict';
/*global $:false */
/*global _:false */
/*global Medium:false */
/* jscs: disable */

angular.module('lists').directive('itemOptions', function($filter, utilityService) {
  return {
    restrict: 'AE',
    link: function(scope, elem, attrs) {

      var $list = $(elem);

      var btnContainer = angular.element('<div style="" class="btn-container list-item-options-container"></div>');
      var btnOptions = angular.element('<button class="btn btn-icon" type="button" aria-expanded="true"><i class="icon-options-b"></i></button>');

      btnContainer.append(btnOptions);


      function _addOptions(element) {
        var index = utilityService.getIndexFromElement(element);
        var item = scope.getItem(index);

        if (!item.divider) {

          element.append(btnContainer);

          btnOptions.off('click').on('click', function(e) {
            e.stopPropagation();
            scope.modals.openItemsModal(index);
          });
        }
      }

      function _removeOptions(element) {
        btnContainer.remove();
      }


      var timer;
      var allowOptionsHover = true;
      $('body').off('mousewheel').on('mousewheel', function(event) {
        if (Math.abs(event.deltaY) <= 2) allowOptionsHover = true;
        else allowOptionsHover = false;
      });

      var lastElement = false;
      var scrollHandler = function() {
        if (lastElement) _addOptions(lastElement);
        allowOptionsHover = true;
      };

      $(window).scroll(function() { // whenever the window is scrolled
        clearTimeout(timer); // cancel the existing timer
        timer = setTimeout(scrollHandler, 300); // and start a new one
      });


      $(elem).off('mouseenter mouseleave mouseover', '.list-item').on('mouseenter', '.list-item', function() {
        lastElement = $(this);
        // if(allowOptionsHover){
        _addOptions($(this));
        // }

      }).on('mouseleave', '.list-item', function() {
        _removeOptions($(this));
      });

    }
  };
});


// one way databinding for contenteditables
angular.module('lists').directive('tempModel', function($filter, utilityService) {
  return {
    restrict: 'AE',
    require: 'ngModel',

    link: function($scope, elem, attrs, ngModel) {

      var titleSelector = '.input-title';
      var descriptionSelector = '.input-content';

      var $list = $(elem);

      $list.on('focus', titleSelector, function() {
        // console.log(ngModel);
        var element = $(this);
        var content = '';
        var index = utilityService.getItemIndex(element);
        element.on('input', function(e) {
          content = element.text();
          ngModel.$modelValue[index].title = content;
        });


      }).on('blur', titleSelector, function() {
        var element = $(this);
        element.off('input');
      });

      $list.on('focus', descriptionSelector, function() {
        // console.log(ngModel);
        var element = $(this);
        var content = '';
        var index = parseInt($(this).attr('data-sub-index'));
        var parentIndex = parseInt($(this).attr('data-item-index'));
        element.on('input', function(e) {
          content = element.html();
          ngModel.$modelValue[parentIndex].content[index].content = content;
        });


      }).on('blur', descriptionSelector, function() {
        var element = $(this);
        element.off('input');
      });
    }
  };
});
