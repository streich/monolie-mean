'use strict';
/*global $:false */
/*global _:false */
/*global key:false */
/* jscs: disable */

angular.module('lists').directive('checkBoxList', function($filter, $timeout, utilityService, globalsService) {
  return {
      restrict: 'AE',
      link: function(scope, elem, attrs) {
        var $list = $(elem);


        $list.on('click','.list-item__header > input',function(){
            var itemIndex = utilityService.getItemIndex($(this).next());
            scope.checkItem(itemIndex);
            console.log('click checkitem');
            scope.$apply();

        });
        $list.on('click','.list-subitem > input',function(){
            var itemIndex = utilityService.getItemIndex($(this).next());
            var subItemIndex = utilityService.getSubItemIndex($(this).next());
            scope.checkSubItem(itemIndex,subItemIndex);
            scope.$apply();
            // all subItems checked ? check item : uncheck item (if checked)
        });
      }
    };

});
