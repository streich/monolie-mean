'use strict';
/*global $:false */
/*global _:false */
/*global key:false */
/* jscs: disable */


var ENTERBLOCKED = false;

angular.module('lists').directive('editableList', function($filter, $timeout, $log, utilityService, globalsService) {
  return {
    restrict: 'AE',
    link: function(scope, elem, attrs) {

      // Define MAC and WINDOWS Shortcuts
      var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
      var SHORTCUTS = {
        DUPLICATE_ITEM: isMac ? 'command+d' : 'alt+shift+d',
        MOVE_ITEM_UP: isMac ? 'command+up' : 'alt+shift+up',
        MOVE_ITEM_DOWN: isMac ? 'command+down' : 'alt+shift+down'
      };

      scope = scope.$parent.$parent;
      var $list = $(elem);
      var titleSelector = '.input-title';
      var descriptionSelector = '.input-content';

      function _focusItemByIndex(index, callback, moveCaretToEnd) {
        $timeout(function() {
          if (index < 0) index = 0;
          if (index >= scope.list.items.length) index = scope.list.items.length - 1;

          var $elem = utilityService.getTitleInputByIndex(index);
          if ($elem) {
            scope.list.itemIndex = index;
            scope.$apply();
            if (moveCaretToEnd) utilityService.moveCaretToEnd($elem);
            utilityService.setFocusTo($elem, callback,!moveCaretToEnd);
          } else {
            $log.log('could not focus:', index);
          }
        }, 0);
      }

      function _focusSubItemByIndex(index, subindex, callback, moveCaretToEnd) {
        $timeout(function() {
          if (subindex === false) {
            subindex = scope.list.items[index].content.length - 1;
          } else if (subindex === -1) {
            subindex = 0;
          }
          var elem = utilityService.getContentInputByIndex(index, subindex);
          if (elem) {
            scope.list.itemIndex = index;

            if ($(elem).parent().hasClass('link')) {
              scope.setSubItemType(index, subindex, 'link-editing');
              utilityService.setFocusTo(elem, callback, !moveCaretToEnd);
            } else {
                utilityService.setFocusTo(elem, callback,!moveCaretToEnd);
            }

          } else {
            $log.warn('could not focus on -- itemIndex:', index + ' subItemIndex: ' + subindex);
          }
        }, 0);
      }

      function _focusDescription() {
        var elem = $('.list-description')[0];
        utilityService.setFocusTo(elem);
      }

      function _focusItemsLastSubItemByIndex(index, moveCaretToEnd) {
        if (index < 0) index = 0;
        _focusSubItemByIndex(index, scope.list.items[index].content.length - 1, false, moveCaretToEnd);
      }

      function _insertItemAt(index, title) {
        scope.addItem(index);
      }

      function _insertItemWithTitle(index, title) {
        var item = {
          title: title,
          content: [{
            content: '',
            type: ''
          }]
        };
        scope.addItem(index, item);
      }

      function _listLocked() {
        return scope.list.locked !== 0;
      }

      function _isEmpty(index) {
        return scope.isItemEmpty(index);
      }

      function _isDivider(index) {
        return scope.isItemDivider(index);
      }

      function _isSpace(keycode) {
        return (keycode === 32); //|| keycode === 9 || keycode === 13
      }

      function _isBackspace(keycode) {
        return (keycode === 8);
      }

      function _isEnter(keycode) {
        return (keycode === 13);
      }


      // kill events when list is locked
      $list.on('paste keyup keydown keypress focus blur', titleSelector, function(e) {
        if (_listLocked()) {
          e.stopPropagation();
          e.preventDefault();
          return;
        }
      }).on('paste keyup keydown keypress focus blur', descriptionSelector, function(e) {
        if (_listLocked()) {
          e.stopPropagation();
          e.preventDefault();
          return;
        }
      });


      // Insert multiple items on paste
      $list.on('paste', titleSelector, function(e) {
        // var content = $(this).val();//e.clipboardData.getData('text/plain');
        var content = e.originalEvent.clipboardData.getData('text/plain');
        var index = utilityService.getIndexFromElement($(this));
        var elements = content.split('\n');
        if (elements.length === 1 || (elements.length === 2 && elements[1] === '')) {
          e.preventDefault();
          document.execCommand('insertHTML', false, elements[0]);
        } else if (elements.length > 1) {
          e.preventDefault();
          document.execCommand('insertHTML', false, elements[0]);
          for (var i = elements.length - 1; i >= 1; i--) {
            if (elements[i] !== '') {
              var item = {
                title: elements[i],
                content: []
              };
              scope.addItem(index, item);
            }
          }
          scope.$apply();
        }
      });

      $list.on('paste', descriptionSelector, function(e) {
        // var content = $(this).val();//e.clipboardData.getData('text/plain');
        var content = e.originalEvent.clipboardData.getData('text/plain');
        var index = utilityService.getIndexFromElement($(this));
        var elements = content.split('\n');
        e.preventDefault();
        document.execCommand('insertHTML', false, content);
        scope.$apply();
      });


      var counting = false;
      var counter = 0;

      // add tags here
      $(elem).on('keydown', titleSelector, function(e) {

        function _addTagAndSetTitle(itemIndex, value, startIndex) {
          var string = value.substring(startIndex + 1, value.length);
          var title = value.substring(0, value.length - string.length - 1);
          var type = 'default';

          if (utilityService.isRatingString(string)) type = 'number';
          else if (utilityService.isTimeString(string)) type = 'time';
          else if (utilityService.isDateString(string)) type = 'date';

          scope.addTagToItem(itemIndex, string, type);
          scope.list.items[itemIndex].title = title; //setting title
          utilityService.setInputValue(utilityService.getTitleInputByIndex(itemIndex), title);
          scope.setItemValue(title);
          scope.$apply();
        }

        if (utilityService.isCaretAtEnd(this) && (_isSpace(e.keyCode) || _isEnter(e.keyCode))) {
          var value = utilityService.getInputValue(this);
          var startIndex = value.lastIndexOf(' #') + 1;
          var itemIndex = utilityService.getIndexFromElement($(this));

          // if(_isSpace(e.keyCode) && startIndex > 0 && value.lastIndexOf(' ') < startIndex){
          // _addTagAndSetTitle(itemIndex,value,startIndex);
          // }else if(_isEnter(e.keyCode) && startIndex > 0){
          if (_isEnter(e.keyCode) && startIndex > 0) {
            ENTERBLOCKED = true;
            e.preventDefault();
            e.stopPropagation();
            _addTagAndSetTitle(itemIndex, value, startIndex);
            utilityService.moveCaretToEnd(this);
            ENTERBLOCKED = false;
          }
        }
      });


      // handle focus and blur for title elements
      $(elem).on('focus', titleSelector, function() {
        $(this).parent().addClass('focus');
        key.setScope('item');
      }).on('blur', titleSelector, function() {
        $(this).parent().removeClass('focus');
        key.setScope('all');
      });

      $(elem).on('focus', descriptionSelector, function() {
        $(this).parent().addClass('focus');
        key.setScope('subItem');
      }).on('blur', descriptionSelector, function() {
        $(this).parent().removeClass('focus');
        key.setScope('all');
      });

      // handle title types
      $(elem).on('keyup', titleSelector, function(e) {
        var index = utilityService.getIndexFromElement($(this));
        var string = utilityService.getInputValue(this);

        if (utilityService.isDivider(string)) {
          scope.setItemAttribute(index, 'divider', true);
        } else {
          scope.setItemAttribute(index, 'divider', false);
        }
        // @todo  Find more elegant solution
        scope.$apply();
      });


      // Item Title Shortcuts
      key('backspace', 'item', function(event) {
        var inputTitle = $(event.target);
        var index = utilityService.getIndexFromElement(inputTitle);

        if (scope.removeItem(index, true)) { // remove item before
          scope.$apply(function() {
            if (!scope.hasSubItems(index - 1)) {
              _focusItemByIndex(index - 1);
            } else {
              _focusSubItemByIndex(index - 1, false, false, true);
            }
          });
          return false;
        } else if (scope.removeItem(index - 1, true)) {

          scope.$apply(function() {
            _focusItemByIndex(index - 1);
          });
          return false;
        }
      });



      key('enter', 'item', function(event) {
        // console.log(event);
        var inputTitle = $(event.target);
        var index = utilityService.getIndexFromElement(event.target);

        if (ENTERBLOCKED) {
          ENTERBLOCKED = false;
          return;
        }
        // if item is divider not empty and caret at the beginning, add item before
        if (_isDivider(index)) {
          // if caret is at beggining, add item before divider
          if (utilityService.isCaretAtStart(inputTitle[0])) {
            index -= 1;
            scope.addItem(index);
          } else if (utilityService.isCaretAtEnd(inputTitle[0])) {
            scope.addItem(index);
            //if caret somewhere inbetween
          } else {
            var newItem = false;
            if (inputTitle[0].tagName !== 'TEXTAREA')
              newItem = {
                title: utilityService.extractContentAfterCaret(inputTitle[0]),
                content: []
              };
            scope.addItem(index, newItem);
          }

          scope.$apply(function() {
            _focusItemByIndex(index + 1);
          });

          // add item before if caret at beginning
        } else if (utilityService.isCaretAtStart(inputTitle[0]) && utilityService.getInputValue(inputTitle[0]) !== '') {
          scope.addItem(index - 1);
          scope.$apply();

        } else {
          var newSubitem = false;

          if (inputTitle[0].tagName !== 'TEXTAREA')
            newSubitem = {
              content: utilityService.extractContentAfterCaret(inputTitle[0]),
              type: ''
            };
          scope.addSubItem(index, -1, newSubitem);
          scope.$apply(function() {
            _focusSubItemByIndex(index, 0);
          });
        }
        return false;
      });

      key('down', 'item', function(event) {
        return _itemMoveCaretDown(event.target, true, utilityService.isCaretAtEnd(event.target));
      });

      key('right, ctrl+right, command+right, alt+right', 'item', function(event) {
        return _itemMoveCaretDown(event.target);
      });

      key('left, ctrl+left, command+left, alt+left', 'item', function(event) {
        return _itemMoveCaretUp(event.target);
      });

      key('up', 'item', function(event) {
        return _itemMoveCaretUp(event.target, true, utilityService.isCaretAtEnd(event.target));
      });

      function _itemMoveCaretDown(element, whenAtStart, moveCaretToEnd) {
        var index = utilityService.getIndexFromElement(element);
        if (utilityService.isCaretAtEnd(element) || (whenAtStart && utilityService.isCaretAtStart(element))) {
          if (!scope.hasSubItems(index)) {
            _focusItemByIndex(index + 1, false, moveCaretToEnd);
          } else {
            _focusSubItemByIndex(index, 0, false, moveCaretToEnd);
          }
          return false;
        }
        return true;
      }

      function _itemMoveCaretUp(element, whenAtEnd, moveCaretToEnd) {
        var index = utilityService.getIndexFromElement(element);
        if (utilityService.isCaretAtStart(element) || (whenAtEnd && utilityService.isCaretAtEnd(element))) {
          if (index === 0) {
            _focusDescription();
          } else if (scope.hasSubItems(index - 1)) {
            _focusItemsLastSubItemByIndex(index - 1, moveCaretToEnd);
          } else {
            _focusItemByIndex(index - 1, false, moveCaretToEnd);
          }
          return false;
        }
        return true;
      }



      key(SHORTCUTS.MOVE_ITEM_UP, 'item', function(event) {
        console.log('ctrl+up', $(event.target));
        if (_listLocked()) return;
        var $target = $(event.target);

        var fromIndex = utilityService.getIndexFromElement(event.target);
        var toIndex = fromIndex - 1;
        scope.moveItemToIndex(fromIndex, toIndex);
        globalsService.blockParser = true;
        scope.$apply(function() {
          _focusItemByIndex(fromIndex - 1, function() { // weird
            globalsService.blockParser = false;
          });
        });
        return false;
      });

      key(SHORTCUTS.MOVE_ITEM_DOWN, 'item', function(event) {
        if (_listLocked()) return;

        var $target = $(event.target);
        var fromIndex = utilityService.getIndexFromElement(event.target);
        var toIndex = fromIndex + 1;
        // console.log(typeof toIndex);
        scope.moveItemToIndex(fromIndex, fromIndex + 1);
        // console.log(fromIndex, toIndex);
        globalsService.blockParser = true;
        scope.$apply(function() {
          _focusItemByIndex(fromIndex + 1, function() { // weird
            globalsService.blockParser = false;
          });
        });
        return false;
      });


      // Duploicate Item
      key(SHORTCUTS.DUPLICATE_ITEM, 'item', function(event) {
        event.stopPropagation();
        event.preventDefault();

        if (_listLocked()) return;
        var itemIndex = utilityService.getItemIndex(event.target);
        scope.duplicateItem(itemIndex);
        scope.$apply(function() {
          _focusItemByIndex(itemIndex, function() { // weird
            globalsService.blockParser = false;
          });
        });
        return false;
      });



      // SUBITEMS

      // Move Subitem up
      key(SHORTCUTS.MOVE_ITEM_UP, 'subItem', function(event) {
        event.stopPropagation();
        event.preventDefault();
        if (_listLocked()) return;
        globalsService.blockParser = true;
        var fromIndex = utilityService.getIndexFromElement(event.target);
        var fromItemIndex = utilityService.getItemIndex(event.target);
        var newIndexes = scope.moveSubItemUp(fromItemIndex, fromIndex);
        scope.$apply(function() {
          _focusSubItemByIndex(newIndexes.itemIndex, newIndexes.subItemIndex, function() {
            globalsService.blockParser = false;
          });
        });
        return false;
      });

      // Move Subitem Down
      key(SHORTCUTS.MOVE_ITEM_DOWN, 'subItem', function(event) {
        if (_listLocked()) return;
        globalsService.blockParser = true;
        var fromIndex = utilityService.getIndexFromElement(event.target);
        var fromItemIndex = utilityService.getItemIndex(event.target);
        var newIndexes = scope.moveSubItemDown(fromItemIndex, fromIndex);
        scope.$apply();
        return false;
      });

      // Duplicate Subitem
      key(SHORTCUTS.DUPLICATE_ITEM, 'subItem', function(event) {
        if (_listLocked()) return;
        var index = utilityService.getSubItemIndex(event.target);
        var itemIndex = utilityService.getItemIndex(event.target);
        scope.duplicateSubItem(itemIndex, index);
        scope.$apply();
        return false;
      });

      //  Backspace remove
      key('backspace', 'subItem', function(event) {
        var inputDescription = $(event.target);
        var index = utilityService.getItemIndex(event.target);
        var subitemIndex = utilityService.getSubItemIndex(event.target);
        var inputTitle = utilityService.getTitleInputByIndex(index);
        // if caret at beginning
        if (utilityService.isCaretAtStart(inputDescription[0])) {

          // & item empty -> remove current subitem
          if (utilityService.getInputValue(inputDescription[0]).length === 0) {
            if (scope.removeSubItem(index, subitemIndex)) {
              // update scope when item has actually been removed
              scope.$apply(function() {
                if (subitemIndex === 0)
                  _focusItemByIndex(index,false,true);
                else
                  _focusSubItemByIndex(index, subitemIndex - 1,false,true);
              });
            } else {
              _focusItemByIndex(index);
            }
            return false;

            // if not empty and first subitem -> merge with item
          } else if (subitemIndex === 0) {
            var value = utilityService.setInputValue(inputTitle, utilityService.getInputValue(inputDescription[0]), true, true);
            scope.setItemValue(index, value); //@TODO: improve! currently writing manually into model
            scope.removeSubItem(index, subitemIndex);

            scope.$apply(function() {
              _focusItemByIndex(index);
            });
            return false;
            // if caret at start and not first subitem
          } else if (subitemIndex > 0) { // if previous item empty -> remove it

            $log.log('caret at start bad subitem index > 0',subitemIndex);

            if (scope.isSubItemEmpty(index, subitemIndex - 1)) {
              scope.removeSubItem(index, subitemIndex - 1);
              scope.$apply();
              return false;
              // if not empty -> merge subitems
            } else {
              event.preventDefault();
              var value2 = utilityService.setInputValue(utilityService.getContentInputByIndex(index, subitemIndex - 1), utilityService.getInputValue(inputDescription[0]), true, true);
              scope.setSubItemValue(index, subitemIndex, value2); //@TODO: improve! currently writing manually into model
              scope.removeSubItem(index, subitemIndex);

              scope.$apply(function() {
                _focusSubItemByIndex(index, subitemIndex - 1);
              });
              return false;
            }
          }
        }
      });

      key('enter', 'subItem', function(event) {
        var index = utilityService.getItemIndex(event.target);
        var subitemIndex = utilityService.getSubItemIndex(event.target);
        var subItem = event.target;

        // Content: Return
        if (utilityService.isCaretAtEnd(subItem)) {
          if (scope.isLastSubItem(index, subitemIndex) && scope.isSubItemEmpty(index, subitemIndex)) {
            scope.removeLastSubItem(index);
            scope.addItem(index);
            scope.$apply();
            _focusItemByIndex(index + 1);
          } else {
            // if next subitem is not empty
            if (!scope.list.items[index].content[subitemIndex + 1] || scope.list.items[index].content[subitemIndex + 1].content !== '')
              scope.addSubItem(index, subitemIndex);

            scope.$apply(function() {
              _focusSubItemByIndex(index, subitemIndex + 1);
            });
          }

          // add subitem before if caret at start
        } else if (utilityService.isCaretAtStart(subItem)) {
          // @TODO add if previous item empty
          scope.addSubItem(index, subitemIndex - 1);
          scope.$apply(function() {
            _focusSubItemByIndex(index, subitemIndex + 1);
          });
        } else {
          var newSubitem = {
            content: utilityService.extractContentAfterCaret(subItem),
            type: ''
          };
          scope.addSubItem(index, subitemIndex, newSubitem);
          scope.$apply(function() {
            _focusSubItemByIndex(index, subitemIndex + 1);
          });
          _focusSubItemByIndex(index, subitemIndex + 1);
        }
        return false;
      });

      // indent
      key('shift+tab', 'item', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var index = utilityService.getIndexFromElement(event.target);
        if (index > 0 && !_isDivider(index - 1)) {
          var newIndexes = scope.joinItems(index, index - 1);
          scope.$apply(function() {
            _focusSubItemByIndex(newIndexes.index, newIndexes.subIndex);
          });
        }

      });

      // unindent
      key('shift+tab', 'subItem', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var index = utilityService.getItemIndex(event.target);
        var subitemIndex = utilityService.getSubItemIndex(event.target);
        var newIndex = scope.splitItemAtSubitemIndex(index, subitemIndex);
        scope.$apply(function() {
          _focusItemByIndex(newIndex);
        });
        return false;
      });


      key('up', 'subItem', function(event) {
        return _subItemMoveCaretUp(event.target, true, !utilityService.isCaretAtStart(event.target));
      });

      key('left, ctrl+left, command+left, alt+left', 'subItem', function(event) {
        return _subItemMoveCaretUp(event.target, false, true);
      });

      key('right, ctrl+right, command+right, alt+right', 'subItem', function(event) {
        return _subItemMoveCaretDown(event.target);
      });

      key('down', 'subItem', function(event) {
        return _subItemMoveCaretDown(event.target, true, !utilityService.isCaretAtStart(event.target));
      });


      function _subItemMoveCaretUp(element, whenAtEnd, moveCaretToEnd) {
        var index = utilityService.getItemIndex(element);
        var subitemIndex = utilityService.getSubItemIndex(element);
        if (utilityService.isCaretAtStart(element) || (whenAtEnd && utilityService.isCaretAtEnd(element))) {
          if (subitemIndex === 0) {
            _focusItemByIndex(index, false, moveCaretToEnd);
          } else {
            _focusSubItemByIndex(index, subitemIndex - 1, false, moveCaretToEnd);
          }
          return false;
        }
        return true;
      }

      function _subItemMoveCaretDown(element, whenAtStart, moveCaretToEnd) {

        var index = utilityService.getItemIndex(element);
        var subitemIndex = utilityService.getSubItemIndex(element);
        if (utilityService.isCaretAtEnd(element) || (whenAtStart && utilityService.isCaretAtStart(element))) {
          if (scope.isLastSubItem(index, subitemIndex) && !scope.isLastItem(index)) {
            _focusItemByIndex(index + 1, false, moveCaretToEnd);
          } else {
            _focusSubItemByIndex(index, subitemIndex + 1, false, moveCaretToEnd);
          }
          return false;
        }
        return true;
      }




      // Handle Link editing
      function _getTypeFromString(string) {
        if (utilityService.isUrlString(string)) return 'link';
        if (utilityService.isDivider(string)) return 'divider';
        if (utilityService.isQuoteString(string)) return 'quote';
        if (utilityService.isCodeString(string)) return 'code';
        return '';

      }

      $list.on('focus', descriptionSelector, function() {
        var done = false;
        var _parseSubItemContentDebounced = _.debounce(function(e) {
          if (done) return;
          // console.log('globalsService.blockParser',globalsService.blockParser);
          if (globalsService.blockParser) return;
          var index = utilityService.getItemIndex(this);
          var subitemIndex = utilityService.getSubItemIndex(this);
          // console.log('parsing',scope.list.items[index].content[subitemIndex]);
          var string = utilityService.getInputValue(this);

          if (utilityService.isUrlString(string)) {
            scope.setSubItemType(index, subitemIndex, 'link-editing'); //2 is link
          } else {
            // console.log('subitem type',_getTypeFromString(string));
            scope.setSubItemType(index, subitemIndex, _getTypeFromString(string)); //2 is link
          }
          scope.$apply();
        }, 300);

        var input = $(this);
        input.on('keydown', _parseSubItemContentDebounced);
        input.on('blur', function() {
          done = true; // hack
          if (globalsService.blockParser) return;

          var index = utilityService.getItemIndex(this);
          var subitemIndex = utilityService.getSubItemIndex(this);
          var string = utilityService.getInputValue(this);

          if (utilityService.isUrlString(string)) {
            scope.setSubItemType(index, subitemIndex, 'link'); //2 is link
            scope.$apply();
          }
          input.off('keydown blur');
        });
      });



      // Description Focus il
      $list.on('blur', descriptionSelector, function() {
        if (globalsService.blockParser) return;

        var index = utilityService.getItemIndex(this);
        var subitemIndex = utilityService.getSubItemIndex(this);
        var string = utilityService.getInputValue(this);

        scope.setSubItemType(index, subitemIndex, _getTypeFromString(string)); //2 is link
        scope.$apply();
      });








    }

  };
});
