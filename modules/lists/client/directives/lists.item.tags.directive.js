'use strict';
/*global $:false */
/*global _:false */


angular.module('lists').directive('itemTags', function($filter, utilityService) {
	return {
		restrict: 'AE',
		link: function(scope, elem, attrs) {

			var $list = $(elem);

			// TAGS
			var btnRemoveTag = $('<button class="tag__remove"><i class="icon-remove"></i></button>');
			$('body').off('mouseenter mouseleave', '.tag').on('mouseenter', '.tag', function() {

				var tag = $(this);
				tag.append(btnRemoveTag);

				_.delay(function() {
					tag.addClass('hover');
				}, 5);
				var itemIndex = tag.closest('.list-item').find('.input-title').attr('data-item-index');
				var tagIndex = tag.index();

				tag.off('click').on('click', function() {
					scope.modals.openTagModal(itemIndex, tagIndex);
				});

				btnRemoveTag.on('click', function(e) {
					e.stopPropagation();
					// console.log('tagIndex', tagIndex);
					// console.log('itemIndex', itemIndex);
					scope.removeTagFromItem(itemIndex, tagIndex);
					scope.$apply();
				});

				if ($(this).hasClass('tag-number')) {

					$(this).off('mousewheel').on('mousewheel', function(event) {
						event.preventDefault();
						// console.log(event.deltaX, event.deltaY, event.deltaFactor);
						// var rating = (parseFloat($(this).text())*10 - event.deltaY) / 10;
						var rating = (parseFloat($('span', this).text()) - event.deltaY / 30).toFixed(1);
						if (rating > 9.9) rating = (10.0).toFixed(1);
						else if (rating < 0.1) rating = (0.0).toFixed(1);
						scope.setItemTagContent(itemIndex, tagIndex, rating);
						scope.$apply();
					});

				} else if ($(this).hasClass('tag-date')) {

					$(this).off('mousewheel').on('mousewheel', function(event) {
						event.preventDefault();
						// console.log(event.deltaX, event.deltaY, event.deltaFactor);
						// var rating = (parseFloat($(this).text())*10 - event.deltaY) / 10;
						var date = new Date($('span', this).text());
						date.setDate(date.getDate() - parseInt(event.deltaY / 3 + 1));
						scope.setItemTagContent(itemIndex, tagIndex, $filter('date')(date, 'MM/dd/yyyy'));
						scope.$apply();
					});

				} else if ($(this).hasClass('tag-time')) {
					$(this).off('mousewheel').on('mousewheel', function(event) {
						event.preventDefault();
						// console.log(event.deltaX, event.deltaY, event.deltaFactor);
						// var rating = (parseFloat($(this).text())*10 - event.deltaY) / 10;
						var diff = parseInt(event.deltaY / 3 + 1);
						var date = new Date('10/01/1984 ' + $('span', this).text());
						date = new Date(date.getTime() + diff * 30000);
						scope.setItemTagContent(itemIndex, tagIndex, $filter('date')(date, 'H:mm'));
						scope.$apply();
					});
				}


			}).on('mouseleave', '.tag', function() {
				btnRemoveTag.remove();
				$(this).removeClass('hover');
			});



		}
	};

});
