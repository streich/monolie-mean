'use strict';
/*global $:false */
/*global _:false */

/**
 * Focus handling for list title and list description.
 */
angular.module('lists').directive('listHeader', function(utilityService,$timeout) {
	return {
		restrict: 'AE',

		link: function(scope, elem, attrs) {
			// @todo  stuff this in utils


			function _focusDescription(){
					var elem =  $('.list-description')[0];
					utilityService.setFocusTo(elem);
			}

			function _focusTitle(){
					var elem =  $('.list-title')[0];
					utilityService.setFocusTo(elem);
			}
			function _focusFirstElement(){
					var elem =  $('.input-title')[0];
					utilityService.setFocusTo(elem);
			}


			$(elem).on('keydown', '.list-title', function(e) {
				if (e.keyCode === 13) {
					e.preventDefault();
				 	_focusDescription();
				} else if (e.keyCode === 40 && utilityService.isCaretAtEnd(this)) {
					_focusDescription();
				}
			});

			$(elem).on('focus', '.list-title, .list-description', function(e) {
				scope.showDescription = true;
				scope.$apply();
			});


			$(elem).on('blur', '.list-title, .list-description', function(e) {
					updateDescriptionVisibility();
			});

			function updateDescriptionVisibility(){
				// if(scope.$parent.$parent.list.description === ''){
					scope.showDescription = false;
					scope.$apply();
				// }
			}

			// $timeout(updateDescriptionVisibility, 500);
			// if(document.activeElement === $('.list-title')){
			// 	$('.list-title').trigger('focus');
			// }

			$(elem).on('keydown', '.list-description', function(e) {

				// List Description: Backspace
				if (e.keyCode === 8 && utilityService.isCaretAtStart(this)) {
					e.preventDefault();
					_focusTitle();
				}
				// List Description: Arrow Down
				else if (e.keyCode === 40 && utilityService.isCaretAtEnd(this) ) {
					_focusFirstElement();
				}
				// List Description: Arrow Up
				else if (e.keyCode === 38 && (utilityService.isCaretAtStart(this) || utilityService.isCaretAtEnd(this))) {
					_focusTitle();

				}
				// List Description: Enter
				else if (e.keyCode === 13) {
					e.preventDefault();
					_focusFirstElement();
				}

			});

		}
	};

});
