'use strict';
/*global $:false */
/*global _:false */
/*global SHIFTPRESSED:false */
/* jscs: disable */

// var KEYCODES = {
// 	8: 'backspace',
// 	9: 'tab',
// 	13: 'enter',
// 	16: 'shift',
// 	17: 'ctrl',
// 	18: 'alt',
// 	20: 'capslock',
// 	27: 'esc',
// 	32: 'space',
// 	33: 'pageup',
// 	34: 'pagedown',
// 	35: 'end',
// 	36: 'home',
// 	37: 'left',
// 	38: 'up',
// 	39: 'right',
// 	40: 'down',
// 	45: 'ins',
// 	46: 'del'
// };


angular.module('lists').filter('sanitize', ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  };
}]);


angular.module('lists').directive('linksClickable', function(utilityService) {
    return {
        restrict: 'A',

        link: function(scope, element, attrs) {


            function _listNotLocked() {
                // console.log('scope.list.locked', (scope.list.locked === 0));
                return (scope.list.locked === 0);
            }

            function _initClick() {

              // add double slash to link if no protocol is set
              // $(element).on('mouseenter', '.link a', function(e) {
              //   var firstTwoChars = $(this).attr('href').substr(0,2);
              //   if(firstTwoChars !== '//' && firstTwoChars !== 'ht'  ){
              //      $(this).attr('href', '//'+ $(this).attr('href'));
              //   }
              // });

                $(element).on('click', '.link a', function(e) {
                    if (window.SHIFTPRESSED) {
                        e.preventDefault();
                        _makeEditable($(this).parent().find('.input-content'));
                        $(this).parent().find('.btn-edit-link').remove();
                    }
                });
            }


            function _makeEditable(listElement, focusDelay) {
                if (!focusDelay) focusDelay = 2;
                var $elem = $(listElement);
                var subIndex = utilityService.getSubItemIndex($elem);
                var itemIndex = utilityService.getItemIndex($elem);
                scope.setSubItemType(itemIndex, subIndex, 'link-editing');
                scope.$apply(function() {
                    window.setTimeout(function() {
                        // utilityService.setFocusTo($elem);
                      utilityService.moveCaretToEnd($elem[0]);
                    }, focusDelay);
                });

            }


            function _reset(listElement) {
              var subIndex = utilityService.getSubItemIndex(listElement);
              var itemIndex = utilityService.getItemIndex(listElement);
                if (utilityService.isUrlString($(listElement).val())) {
                    scope.setSubItemType(itemIndex, subIndex, 'link');
                } else {
                    scope.setSubItemType(itemIndex, subIndex, '');

                }
                scope.$apply();
            }

            function _initHover() {
                var btnOptions = $('<button class="btn btn-icon btn-edit-link" type="button" aria-expanded="true"><i class="icon-edit"></i></button>');
                var allowRemoval = true;
                var timer = false;

                $(element).on('mouseenter', '.link', function() {
                    allowRemoval = true;
                    if (_listNotLocked()) {
                        var _this = this;
                        $(this).append(btnOptions);
                        btnOptions.off('click mouseenter mouseleave').on('click', function() {
                            _makeEditable($('.input-content', _this)[0], 100);
                        }).on('mouseenter', function() {
                            allowRemoval = false;
                            // console.log('mouseenter options', allowRemoval);
                        }).on('mouseleave', function() {
                            allowRemoval = true;
                            _mouseleave();
                            // console.log('mouseleave options', allowRemoval);
                        });
                    }
                }).on('mouseleave', '.link', function() {
                    _mouseleave();
                }).on(' blur focusout', '.link .input-content', function() {
                    _reset(this);
                });

                function _mouseleave() {
                    if (allowRemoval)
                        btnOptions.remove();
                }


            }

            // if(!scope.list.locked !== ß0){
            _initClick();
            _initHover();
            // }


        }
    };
});






/*
This directive allows us to pass a function in on an enter key to do what we want.
 */
angular.module('lists').directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind('keydown keypress', function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});


// @TODO Move this to core
angular.module('lists').directive('matchWindowHeight', function($timeout, $window) {
    var cleanup, setHeight;

    return function(scope, element, attrs) {
        function setHeight() {
            return element.css('height', $window.innerHeight + 'px');
        }
        setHeight();

        $timeout(function() {
            return setHeight();
        });

        angular.element($window).on('resize', setHeight);

        function cleanup() {
            return angular.element($window).off('resize', setHeight);
        }

        element.on('$destroy', cleanup);
    };
});





// @TODO Move this to core
angular.module('lists').directive('selectLists', function($timeout, $window) {
    var cleanup, setHeight;

    return function(scope, element, attrs) {
// data-ng-click="openSettingsModalByListID(list._id);  $event.stopPropagation(); $event.preventDefault();"
// ng-class="{active: $stateParams.listId == list._id}"
// data-ng-click="goToList(list._id); "
    var $list = $(element);

    $timeout(function () {

    var currentElement = $list.find('[data-list-id="'+scope.$stateParams.listId+'"]');
    if(currentElement.length > 0) currentElement.addClass('active');
    });

    $list.on('click', '.category-item', function(e) {
        e.stopPropagation();
        e.preventDefault();
        scope.goToList($(this).attr('data-list-id'));

        $list.find('.category-item.active').removeClass('active');
        $(this).addClass('active');
    });

    $list.on('click', '.category-item__options', function(e) {
        e.stopPropagation();
        e.preventDefault();
        scope.openSettingsModalByListID($(this).closest('.category-item').attr('data-list-id'));
    });

    };
});
