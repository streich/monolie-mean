'use strict';
/* global $, _ */



angular.module('lists').directive('autosaveCategory',[
	function () {
	    function autosaveCategoryController($scope, $element, $attrs) {
	        function saveModel(newModel,oldModel) {
	        	console.log('autosaveCategory',$scope.currentCategory);
	        	if(!_.isUndefined($scope.$root.saveCategory)){
	        		$scope.$root.saveCategory($scope.currentCategory);
	        	}
	        }

	        var saveModelDebounced = _.debounce(saveModel, 300);

	        $scope.$watch('currentCategory.marker', saveModelDebounced, true);
	        $scope.$watch('currentCategory.name', saveModelDebounced, true);

	    }

	return {
	    restrict : 'A',
	    link: autosaveCategoryController
	};

}]);
