'use strict';
/*global $:false */
/*global _:false */
/*global smartdate:false */
/* jscs: disable */
/*global key:false */



// Lists controller
angular.module('lists')
    .constant('LIST_MODES', [{
      id: 0,
      name: 'default',
      css: 'list'
    }, {
      id: 1,
      name: 'columns',
      css: 'columns'
    }, {
      id: 2,
      name: 'slideshow',
      css: 'slideshow'
    }, {
      id: 3,
      name: 'checkboxes',
      css: 'todo'
    }, {
      id: 4,
      name: 'test',
      css: 'list'
    }])
    .controller('ListsController', ['$scope', '$rootScope', '$state', 'autoSaveService',
    '$stateParams', '$location', '$timeout', 'LIST_MODES', 'CATEGORY_ICONS',
    'Authentication', 'Lists', 'ListsFavorites', 'ListsPublic', 'ListsRecent',
    'ListsByCategory','listsEditService', '$modal', '$log', 'modalService',
        function($scope, $rootScope, $state, autoSaveService, $stateParams, $location, $timeout,
          LIST_MODES, CATEGORY_ICONS, Authentication, Lists, ListsFavorites,
          ListsPublic, ListsRecent, ListsByCategory, listsEditService, $modal, $log,
          modalService) {
            $scope.authentication = Authentication;

          if (!$scope.authentication.user) {
            $location.path('/authentication/signin');
            }

            $scope.modes = LIST_MODES;
            $scope.CATEGORY_ICONS = CATEGORY_ICONS;
            $rootScope.currentListMode = LIST_MODES[0];
            if(!$stateParams.listId) $rootScope.listItemsLoaded  = true;

            $scope.listMode = '';
            // $scope.blockApply = false;

            $scope.config = {};
            $scope.config.itemsDisplayedInList = 200;

            $scope.$stateParams = $stateParams;
            $scope.list = false;


            // handle autosave
            autoSaveService.setCallback(
              function(){
                $scope.save();
              }
            );
            $scope.$watch('list',function(newVal,oldVal){
                autoSaveService.triggerDebounced();
            },true);


            // @TODO move this to directive
            //jQuery time
            $(document).on('mouseup','a',function(e){
              var parent, ink, d, x, y;
            	parent = $(this).parent();
            	//create .ink element if it doesn't exist
            	if(parent.find('.ink').length === 0)
            		parent.prepend('<span class="ink"></span>');

            	ink = parent.find('.ink');
            	//incase of quick double clicks stop the previous animation
            	ink.removeClass('animate');

            	//set size of .ink
            	if(!ink.height() && !ink.width())
            	{
            		//use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
            		d = Math.max(parent.outerWidth(), parent.outerHeight());
            		ink.css({height: d, width: d});
            	}

            	//get click coordinates
            	//logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
            	x = e.pageX - parent.offset().left - ink.width()/2;
            	y = e.pageY - parent.offset().top - ink.height()/2;

            	//set the position and add class .animate
            	ink.css({top: y+'px', left: x+'px'}).addClass('animate');
            });

            // overwrite keymasters filter to allaw save shortcut in inputs
            key.filter = function(event){
              var tagName = (event.target || event.srcElement).tagName;
              return true;
            };

            // OTHER SHORTCUTS
            // SAVE ON STRG + S
            key('command+s', 'all', function(event){
                  console.log('save');
                  event.preventDefault();
                  $scope.update();
                  return false;
            });


            // NEW LIST ON  N
            // DOES NOT WORK
            key('command+n', 'all', function(event){
                  console.log('new list');
                  event.preventDefault();
                  $scope.create(false, true);
                  return false;
            });

            // default list
            $scope.getCurrentListModeAsString = function() {
                // console.log('$scope.list.mode', $scope.list);
                if (!$scope.list.mode) $scope.list.mode = 0;
                return LIST_MODES[$scope.list.mode].name;
            };


            // @TODO move this somewehere else
            smartdate.locale.en.pastShort = function(date, fullMonthNames) {
                var now = smartdate.now();
                var sec = (now.getTime() - date.getTime()) / 1000;

                if (sec < 60) {
                    return 'just now';
                } else if (sec < 60 * 60) {
                    return Math.floor(sec / 60) + ' min ago';
                } else if (smartdate.daysBetween(now, date) === 0) {
                    return this.time(date);
                } else if (smartdate.daysBetween(now, date) === -1) {
                    return 'yesterday';
                }
                return this.date(date, fullMonthNames);
            };

            $scope.formatDate = function(dateAsString) {
                return smartdate.format(new Date(dateAsString), {
                    mode: 'pastShort',
                    capitalize: true
                });

            };

            $scope.createEmptyList = function() {
                // console.log('$stateParams.catId ', $stateParams.catId);
                return new Lists({
                    name: 'New List',
                    favorite: false,
                    description: '',
                    share: false,
                    itemIndex: 0,
                    todo: false,
                    locked: 0,
                    category: $stateParams.catId ? _.findWhere($scope.categories, {
                        _id: $stateParams.catId
                    }) : null,
                    // config:         {autoTag:['rating','date','datetime']},
                    mode: 0,
                    count: 1,
                    items: [{
                        title: '',
                        content: []
                    }, ]
                });
            };
            $scope.list = $scope.createEmptyList(); // @TODO what was this for again?

            $scope.addMoreItems = function() {
                if ($scope.list.items && $scope.config.itemsDisplayedInList < $scope.list.items.length) {
                    $scope.config.itemsDisplayedInList = $scope.config.itemsDisplayedInList + 4;
                }
            };

            $scope.listValid = function() {
                return $stateParams.listId && $stateParams.listId !== 'all';
            };


            $scope.listLocked = function() {
                return $scope.list.locked !== 0;
            };

            // used by lists in category
            $scope.goToList = function(listId) {
                $rootScope.$broadcast('leaveList');
                var state = $state.current.name;
                $rootScope.listItemsLoaded = false;
                $state.go(state, {
                    listId: listId
                }, {
                    notify: false
                });
                $scope.findOne(listId);
            };


            // Create new List
            $scope.save = function() {
                $scope.update();
            };

            // refactor
            $scope.create = function(list, redirect, refresh, index) {
                $rootScope.listItemsLoaded = false;

                // Create new List object
                if (!list) {
                    list = $scope.createEmptyList();
                }
                $scope.list = list;

                // Redirect after save
                list.$save(function(response) {
                    // $scope.findRefresh();
                    if (!index)
                        $scope.lists.unshift(response);
                    else
                        $scope.lists.splice(index, 0, response);

                    if ($state.current.name === 'listsCategorized')
                        $scope.listsCategorized = $scope.categorizeLists($scope.lists);
                    // redirect to list detail view or not
                    if (redirect) {
                        $scope.goToList(response._id);
                    }

                    // refresh $scope.lists from db or not
                    if (refresh) {
                        $scope.findRefresh();
                    }
                    return false;
                }, function(errorResponse) {
                    $scope.error = errorResponse.data.message;
                });

            };



            // Update existing List
            $scope.update = function(list) {
                var templist = _.clone(list);

                if (!templist) {
                    templist = _.clone($scope.list);
                }

                if (!templist) return;
                if (templist.category)
                    templist.category = templist.category._id;

                var index = (function(){
                  for (var i = 0; i < $scope.lists.length; i++) {
                    if ($scope.lists[i]._id === templist._id) {
                      return i;
                    }
                  }
                  return 0;
                })();

                // templist.updated = new Date();
                templist.count = templist.items.length;
                var activeID = document.activeElement.getAttribute('id');
                // console.log('_version before:',  $scope.list.__v);
                templist.$update(function(response) {
                    $scope.listMode = 'edit';

                    // nice hack, mate. adding the version to the cloned list ensures,
                    $scope.list.__v = response.__v;

                    // console.log('_version after:',  $scope.list.__v);
                    if(templist.description) $scope.lists[index].height = 55;
                    else $scope.lists[index].height = 40;

                    // apply new name and description tolist
                    var itemInList = _.findWhere($scope.lists, {
                        _id: response._id
                    });
                    if (itemInList) {
                        itemInList.name = response.name;
                        itemInList.description = response.description;
                        itemInList.count = response.items.length;
                        itemInList.updated = response.updated;
                        itemInList.favorite = response.favorite;
                        itemInList.locked = response.locked;
                        itemInList.category = $scope.list.category;
                    }

                    if ($state.current.name === 'listsCategorized')
                        $scope.listsCategorized = $scope.categorizeLists($scope.lists);
                        // update list of lists
                        $timeout(function(){
                          $rootScope.$broadcast('vsRepeatTrigger');
                      }, 10);
                }, function(errorResponse) {
                    // $scope.error = errorResponse.data.message;
                    console.warn('could not update:',templist);
                  });

            };

            // add list item
            $scope.addItem = function(index, item) {
                if (!item) {
                    item = {
                        title: '',
                        content: []
                    };
                }

                if (index === false || index === 'undefined') {
                    $scope.list.items.push(item);
                } else {
                    $scope.list.items.splice(index + 1, 0, item);
                }
            };

            $scope.resetInfiniteScroll = function() {
                $scope.config.itemsDisplayedInList = 30;
            };

            //@todo  put them together
            $scope.setItemTagContent = function(itemIndex, tagIndex, content) {
                $scope.list.items[itemIndex].tags[tagIndex].content = content;
            };

            $scope.setItemTagType = function(itemIndex, tagIndex, type) {
                $scope.list.items[itemIndex].tags[tagIndex].type = type;
            };

            $scope.addTagToItem = function(itemIndex, content, type) {
                if (!$scope.list.items[itemIndex].tags) $scope.list.items[itemIndex].tags = [];
                if (!type) type = 'default';

                $scope.list.items[itemIndex].tags.push({
                    content: content,
                    type: type
                });
            };

            $scope.removeTagFromItem = function(itemIndex, tagIndex) {
                if ($scope.list.items[itemIndex].tags) {
                    $scope.list.items[itemIndex].tags.splice(tagIndex, 1);
                }
            };

            $scope.getTagFromItem = function(itemIndex, tagIndex) {
                return $scope.list.items[itemIndex].tags[tagIndex];
            };

            $scope.getSubItem = function(itemIndex, index) {
                return $scope.list.items[itemIndex].content[index];
            };

            $scope.getItem = function(index) {
                return ($scope.list.items[index]);
            };

            $scope.isLastItem = function(itemindex) {
                return ($scope.list.items.length - 1 === itemindex);
            };


            $scope.checkSubItem = function(itemIndex,subItemIndex){
              // all subItems checked ? check item : uncheck item (if checked)
              var item = $scope.list.items[itemIndex];
              function _checked(element, index, array){
                return element.checked;
              }
                item.checked = item.content.every(_checked);
            };

            $scope.checkItem = function(itemIndex){
              // checked ? check all subitems : check all items
              var item = $scope.list.items[itemIndex];
              function setChecked(element, index, array) {
                element.checked = item.checked;
              }
              item.content.forEach(setChecked);
            };

            $scope.hasSubItems = function(itemindex) {
                // return !$scope.isItemDivider(itemindex) && $scope.list.items[itemindex].content.length > 0;
                try{
                  return $scope.list.items[itemindex].content.length > 0;
                }catch(e){
                  console.warn('$scope.hasSubItems - item not found:',itemindex);
                  console.warn($scope.items);
                  console.warn($scope);
                  return false;
                }
            };

            $scope.isSubItemEmpty = function(itemindex, index) {
              try{
                return ($scope.list.items[itemindex].content[index].content === '');
              }catch(e){
                console.warn('$scope.isSubItemEmpty - item not found:',itemindex);
                console.warn($scope.list);
                console.warn($scope);

                return false;
              }
            };

            $scope.isLastSubItem = function(itemindex, index) {
                try{
                  return ($scope.list.items[itemindex].content.length - 1 === index);
                }catch(e){
                  console.warn('$scope.isLastSubItem - item not found:',itemindex);
                  console.warn($scope.list);
                  console.warn($scope);

                  return false;
                }
            };

            $scope.removeLastSubItem = function(itemindex) {
                if ($scope.list.items[itemindex].content.length > 0) {
                    $scope.list.items[itemindex].content.pop();
                }
            };

            // add mulitple subitems
            $scope.addSubItems = function(itemindex, index, subitems) {
                if (subitems.length > 0) {
                    if (index === -1 || index === false || index === 'undefined') index = $scope.list.items[itemindex].content.length - 1;
                    // $scope.list.items[itemindex].content.splice(index+1,0,subitems);
                    Array.prototype.splice.apply($scope.list.items[itemindex].content, [index + 1, 0].concat(subitems));
                }
            };

            // add single subitem
            $scope.addSubItem = function(itemindex, index, subitem) {
                if (!subitem) {
                    subitem = {
                        content: '',
                        type: ''
                    };
                }

                if (index === false || index === 'undefined') {
                    $scope.list.items[itemindex].content.push(subitem);
                } else {
                    $scope.list.items[itemindex].content.splice(index + 1, 0, subitem);
                    $scope.subFocusIndex = index + 1;
                }

                // @TODO find other solution?
                $scope.checkSubItem(itemindex,0);
                return subitem;
            };

            // splits subitems and creates a new item
            $scope.splitItemAtSubitemIndex = function(index, subIndex) {
                function _createNewItemFromSubitems() {
                    var subItem = $scope.list.items[index].content[subIndex];
                    var subitems = $scope.list.items[index].content.splice(subIndex, $scope.list.items[index].content.length - subIndex);
                    subitems.shift(); // remove First element
                    var newItem = {
                        title: subItem.content,
                        checked: subItem.checked,
                        content: subitems
                    };
                    if(subItem.tags) newItem.tags = subItem.tags;
                    return newItem;
                }
                $scope.addItem(index, _createNewItemFromSubitems());
                return index + 1;
            };

            $scope.mergeSubItemWithItem = function(itemIndex, subItemIndex) {
              // var item = $scope.list.items[itemIndex];
              var subItem = $scope.list.items[itemIndex].content[subItemIndex];
              $scope.list.items[itemIndex].title =  $scope.list.items[itemIndex].title + subItem.content;
              console.log('item.title', $scope.list.items[itemIndex].title);
              $scope.removeSubItem(itemIndex, subItemIndex);
            };


           $scope.mergeSubItems = function(itemIndex, subItemIndexA,subItemIndexB) {
                var subItemA = $scope.list.items[itemIndex].content[subItemIndexA];
                var subItemB = $scope.list.items[itemIndex].content[subItemIndexB];

                var mergedSubitem = {content: subItemA.content + subItemB.content};
                $scope.removeSubItem(itemIndex, subItemIndexB);
                $scope.removeSubItem(itemIndex, subItemIndexA);
                $scope.addSubItem(itemIndex, subItemIndexA-1, mergedSubitem);
                return {itemIndex:itemIndex, subItemIndex: subItemIndexA-1};
              };

            // merges one item into another
            $scope.joinItems = function(fromIndex, toIndex) {
                var fromItem = $scope.list.items[fromIndex];
                var newSubItem = {
                    content: fromItem.title,
                    type: ''
                };

                if(fromItem.tags) newSubItem.tags = fromItem.tags;
                $scope.addSubItem(toIndex, false, newSubItem);
                var focusIndex = $scope.list.items[toIndex].content.length - 1;

                // add current subitems to previous item as well :)
                $scope.addSubItems(toIndex, false, $scope.list.items[fromIndex].content);
                $scope.removeItem(fromIndex);
                return {
                    index: fromIndex > toIndex ? toIndex : toIndex - 1,
                    subIndex: focusIndex
                };
            };

            $scope.setItemValue = function(itemindex, value) {
                if ($scope.list.items[itemindex]) $scope.list.items[itemindex].title = value;
            };

            $scope.setSubItemValue = function(itemindex,index, value) {
              if ($scope.list.items[itemindex].content[index]) $scope.list.items[itemindex].content[index].content = value;
            };

            // type : 2 = link
            $scope.setSubItemType = function(itemindex, index, type) {
                if (!type) type = false;
                if ($scope.list.items[itemindex].content[index]) $scope.list.items[itemindex].content[index].type = type;
            };

            // Remove existing List item
            $scope.removeSubItem = function(itemIndex, index, soft, focusOffset) {
                if (index < 0) return false;

                var allow = true;
                var item = $scope.list.items[itemIndex].content[index];
                // if(!focusOffset) focusOffset = -1;

                if (soft) {
                    //allow if content is empty
                    if (item.content === '') {
                        allow = true;
                    } else {
                        allow = false;
                    }
                }
                // do not remove when content has only one item left
                if ($scope.list.items[itemIndex].content.length === 0) {
                    allow = false;
                }
                // remove the beast; or not
                if (allow) {
                    $scope.list.items[itemIndex].content.splice(index, 1);
                }
                return allow;
            };

            $scope.removeItems = function(indexes, checkIfEmpty, focusOffset) {
                indexes = indexes.sort();
                for (var i = indexes.length - 1; i >= 0; i--) {
                    $scope.removeItem(indexes[i], checkIfEmpty);
                }
            };

            $scope.isItemEmpty = function(index){
                try{
                  var item = $scope.list.items[index];
                  return (item.title === '' && (item.content === '' || (angular.isObject(item.content) && item.content.length === 0 || (item.content.length === 1 && item.content[0].content === ''))));
                }catch(e){
                  console.warn('$scope.isItemEmpty - item not found:',index);
                  console.warn($scope.list);
                  return true;
                }

            };
            // Remove existing List item
            $scope.removeItem = function(index, checkIfEmpty, focusOffset) {
                if (index < 0) return false;

                var allow = true;
                var item = $scope.list.items[index];
                if (!focusOffset) focusOffset = -1;

                if (checkIfEmpty) {
                    if ($scope.isItemEmpty(index)) {
                        allow = true;
                    } else {
                        allow = false;
                    }
                }
                if ($scope.list.items.length === 1) {
                    allow = false;

                }
                if (allow) {
                    $scope.list.items.splice(index, 1);
                }
                return allow;
            };

            $scope.duplicateItem = function(index) {
                var duplicatedItem = angular.copy($scope.list.items[index]);
                $scope.list.items.splice(index+1, 0, duplicatedItem);
            };

             $scope.duplicateSubItem = function(index,subIndex) {
                var duplicatedItem = angular.copy($scope.list.items[index].content[subIndex]);
                if(duplicatedItem.type === 'link-editing') duplicatedItem.type = 'link';
                $scope.list.items[index].content.splice(subIndex+1, 0, duplicatedItem);
            };

            $scope.moveItemToIndex = function(fromIndex, toIndex) {
                if (toIndex < 0 || toIndex > $scope.list.items.length) return false;
                $scope.list.items.splice(toIndex, 0, $scope.list.items.splice(fromIndex, 1)[0]);
            };

            $scope.moveItemToEnd = function(fromIndex) {
                $scope.moveItemToIndex(fromIndex, $scope.list.items.length);
            };

            $scope.moveSubItemUp = function(fromItemIndex, fromIndex) {
                var toItemIndex = fromItemIndex;
                var toIndex = fromIndex - 1;
                console.log('fromIndex: ' + fromIndex + ' toIndex: ' + toIndex + ' toItemIndex: ' + toItemIndex + ' ');
                if (toIndex < 0) {
                    // skip divider items
                    for(var i = 1; i < toItemIndex + 1;  i++){
                        if(!$scope.isItemDivider(toItemIndex - i)){
                            toItemIndex = toItemIndex - i;
                            toIndex = false; // if toIndex is false, it will be added as last item
                            break;
                        }
                    }
                }
                return  $scope.moveSubItemToIndex(fromItemIndex, toItemIndex, fromIndex, toIndex);
            };


            $scope.moveSubItemDown = function(fromItemIndex, fromIndex) {
                    var toItemIndex = fromItemIndex;
                    var toIndex = fromIndex + 1;
                    console.log('fromIndex: ' + fromIndex + ' toIndex: ' + toIndex + ' toItemIndex: ' + toItemIndex + ' ');

                    if ($scope.isLastSubItem(fromItemIndex, fromIndex)) {
                            //skip divider items
                          for(var i = 1; i <  $scope.list.items.length- toItemIndex;  i++){
                            if(!$scope.isItemDivider(toItemIndex + i)){
                                toItemIndex = toItemIndex + i;
                                toIndex = 0;
                                break;
                            }
                        }
                    }
                    return  $scope.moveSubItemToIndex(fromItemIndex, toItemIndex, fromIndex, toIndex);
            };


            $scope.moveSubItemToIndex = function(fromItemIndex, toItemIndex, fromIndex, toIndex) {
                // console.log('moveSubItemToIndex - fromItemIndex: ' + fromItemIndex + ' toItemIndex: ' + toItemIndex +' fromIndex: ' + fromIndex +  '  toIndex: ' + toIndex);
                if (toItemIndex === false) toItemIndex = fromItemIndex;

                var fromSubitems = $scope.list.items[fromItemIndex].content;
                var toSubitems = $scope.list.items[toItemIndex].content;
                if (toItemIndex < 0 || toItemIndex > toItemIndex.length) return false;


                if (toIndex < 0) {
                    // push to end if not defined or below 0
                    toSubitems.unshift(fromSubitems.splice(fromIndex, 1)[0]); // @TODO THIS CAUSES TROUBLE  SOMETIMES
                } else if (toIndex === false) {
                    toSubitems.push(fromSubitems.splice(fromIndex, 1)[0]);
                } else {
                    toSubitems.splice(toIndex, 0, fromSubitems.splice(fromIndex, 1)[0]);
                }

                return {itemIndex:toItemIndex,subItemIndex:toIndex};
            };

            // $scope.getDividers = function(){
            // 	var dividers = [];
            // };

            // $scope.moveItemToFinishedDivider = function(index, divider ) {};
            // $scope.moveItemToDivider = function(index, divider ) {};

            $scope.moveItemToList = function(index, listId) {
                var list = Lists.get({
                    listId: listId
                }, function() {
                    list.items.push($scope.getItem(index));

                    list.$update(function(response) {
                        $scope.removeItem(index);
                        return false;
                    }, function(errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });

                });
            };

            $scope.moveItemsToList = function(indexes, listId) {
                $scope.findList(listId, function(list) {
                    angular.forEach(indexes, function(key, value) {
                        list.items.push($scope.getItem(key));
                    });
                    list.$update(function(response) {
                        $scope.removeItems(indexes);
                        return false;
                    }, function(errorResponse) {
                        $scope.error = errorResponse.data.message;
                    });

                });
            };



            $scope.findList = function(listId, callback) {
                var list = Lists.get({
                    listId: listId
                }, function() {
                    callback(list);

                });
            };



            $scope.duplicateList = function(list) {
                var newList = new Lists({
                    name: list.name + ' (Duplicate)',
                    favorite: list.favorite,
                    todo: list.todo,
                    ratingmode: list.ratingmode,
                    mode: list.mode,
                    description: list.description,
                    locked: list.locked,
                    share: list.share,
                    itemIndex: list.itemIndex,
                    category: list.category,
                    count: list.items.length,
                    items: list.items
                });
                $scope.create(newList, true, false, $scope.lists.indexOf(list));
            };

            $scope.remove = function(list) {
                // console.log('remove',list);
                //
                // if ( !$scope.listMode) { // if list not in detail view
                list.$remove({
                    listId: list._id
                }, function() {
                    // $scope.findRefresh(); // this is not so nice
                    for (var i in $scope.lists) {
                        if ($scope.lists[i]._id === list._id) {
                            $scope.lists.splice(i, 1);
                        }
                    }
                    // if list currently active
                    if ($scope.list._id === list._id) {
                        var state = $state.current.name;
                        $state.go(state, {
                            listId: ''
                        }, {
                            notify: true
                        });
                        list = false;
                    }
                });

                // } else {
                // // when in list detail view?
                // $scope.list.$remove(function() {
                // 	$location.path('lists');
                // });
                // }
            };


            $scope.setItemAttribute = function(itemIndex, attribute, value) {
                $scope.list.items[itemIndex][attribute] = value;
            };

            $scope.isItemDivider = function(index) {
              try{
                var item = $scope.list.items[index];
                return (item.divider);
              }catch(e){
                console.warn('$scope.isItemDivider - item not found:',index);
                console.warn($scope.list.items);
                return false;
              }
            };
            // $scope.isItemEmpty = function(index) {
            //     var item = $scope.list.items[index];
            //     console.log('isItemEmpty');
            //     console.log(item);
            //     console.log((item.title === '' && item.content === ''));
            //     return (item.title === '' && item.content === '');
            // };



            // Find a list of Lists
            $scope.find = function(type) {
              $rootScope.listItemsLoaded = false;

                function _onListsLoaded(){
                    $timeout(function(){
                        $rootScope.listsLoaded = true;
                    },300);
                }

                $scope.currentListType = type;
                $scope.listMode = false;
                switch (type) {
                    case 'recent':
                        $scope.lists = ListsRecent.query(_onListsLoaded);
                        break;
                    case 'categorized':
                        $scope.findAndCategorize(_onListsLoaded);
                        break;
                    case 'category':
                        $scope.lists = ListsByCategory.query({
                            catId: $stateParams.catId
                        }, function() {
                            console.log('$stateParams.catId', $stateParams.catId);
                            $scope.currentCategory = $scope.findByCategoryById($stateParams.catId);
                            _onListsLoaded();
                        });
                        break;
                    case 'public':
                        $scope.lists = ListsPublic.query(_onListsLoaded);
                        break;
                    case 'favorites':
                        // $rootScope.favoriteLists = ListsFavorites.query();
                        $scope.lists = ListsFavorites.query(_onListsLoaded);
                        break;
                    default:
                        $scope.lists = Lists.query(_onListsLoaded);
                }
            };

            $scope.findRefresh = function() {
                console.log('findRefresh', $scope.currentListType);
                $scope.find($scope.currentListType);
            };


            $scope.findAndCategorize = function(callback) {
                var lists = Lists.query(function(req) {
                    $scope.lists = req;
                    $scope.listsCategorized = $scope.categorizeLists(req);
                    if(callback) callback();
                });
            };

            // not used yet
            $scope.concatLists = function(listsCategorized) {
                var concatenatedLists = [];
                angular.forEach(listsCategorized, function(category, key) {
                  concatenatedLists.push({
                      name: category.name,
                      marker: category.marker,
                      divider: true
                  });
                  concatenatedLists.concat(category);
                });

                return listsCategorized;
            };

            $scope.categorizeLists = function(lists) {
                var listsCategorized = {
                    'uncategorized': {
                        name: 'uncategorized',
                        items: []
                    }
                };
                angular.forEach(lists, function(item, key) {
                    if (!item.category || item.category === 'null' || item.category === 'undefined') item.category = {
                        name: 'uncategorized'
                    };
                    if (!listsCategorized[item.category.name]) {
                        listsCategorized[item.category.name] = {
                            name: item.category.name,
                            marker: item.category.marker,
                            _id: item._id,
                            items: []
                        };
                    }
                    listsCategorized[item.category.name].items.push(item);
                });
                return listsCategorized;
            };


            // Find existing List
            $scope.findOne = function(listId) {
                // $scope.list =
                if (!listId) listId = $stateParams.listId;
                if (!listId) return false;

                var list = Lists.get({
                    listId: listId
                }, function() {
                    // prepocess all
                    angular.forEach(list.items, function(value, key) {
                        if (!angular.isArray(value.content)) {
                            value.content = [{
                                content: value.content
                            }];
                            // }else if(value.content.length === 0){
                            // 	value.content = [{content:''}];
                        }
                    });

                     $timeout(function(){
                        $rootScope.listItemsLoaded = true;
                    },300);

                    if (!list.locked) list.locked = 0;
                    $scope.carouselIndex = 0;
                    $scope.list = list;
                    if ($rootScope) $rootScope.currentListMode = $scope.getCurrentListModeAsString();
                    console.log('findOne', $scope.list);

                });
            };

            $scope.findByCategoryById = function(catId) {
                console.log('findByCategoryById', $scope);
                return _.find($scope.categories, function(item) {
                    return item._id === catId;
                });
            };

            // Opens the settings modal
            $scope.openSettingsModalByListID = function(listID) {
                if (listID) {
                    if (listID === $scope.list._id) {
                        $scope.openSettingsModal();
                    } else {

                        $scope.findList(listID, function(list) {
                            $scope.openSettingsModal(list, $scope);
                        });
                    }
                }
            };

            $scope.openSettingsModal = function(list) {
                if (!list) list = $scope.list;
                modalService.showModalListOptions(list, $scope).then(function(listFromModal) {
                    if (listFromModal) $scope.update(listFromModal);
                });
            };

            $scope.openModesModal = function(list) {
                if (!list) list = $scope.list;
                modalService.showModalListModes(list, $scope).then(function(listFromModal) {
                    if (listFromModal) $scope.update(listFromModal);
                });
            };

            $scope.modals = {};
            $scope.modals.openCategoriesModal = function(list) {
                if (!list) list = $scope.list;
                modalService.showModalListCategories(list, $scope.categories,$scope).then(function(catId) {
                    console.log('openCategoriesModal', catId);
                    // list.category = list.category;
                    if (catId) {
                        list.category = _.findWhere($scope.categories, {
                            _id: catId
                        });
                        // list.category = catId;
                        $scope.update(list);
                    }
                });
            };

            $scope.modals.openItemsModal = function(itemIndex) {
                modalService.showModalListItem($scope, itemIndex).then(function(list) {
                });
            };


            $scope.modals.openTagModal = function(itemIndex, tagIndex) {
                // if(!list) list = $scope.list;
                var tag = $scope.getTagFromItem(itemIndex, tagIndex);
                if (tag.type && tag.type === 'number') {
                    modalService.showModalTagNumber($scope, itemIndex, tagIndex);
                } else if (tag.type && tag.type === 'date') {
                    modalService.showModalTagDate($scope, itemIndex, tagIndex);
                } else {
                    modalService.showModalTagDefault($scope, itemIndex, tagIndex);
                }
            };

           $scope.modals.openExportModal = function(list) {
                  modalService.showModalListExport(list).then(function() {

                });
            };


           $scope.modals.openImportModal = function(list) {
                  modalService.showModalListImport(list).then(function(items) {
                        ///merge lists here.
                        ///
                    if(items) $scope.list.items = items;
                });
            };


            $scope.exportAll = function() {
                var all = Lists.get({
                    listId: all
                }, function() {

                });
            };

        }
    ]);




// DEBUGGING ACCESS TO SCOPE
// var $debugScope = false;
// setTimeout(function(){
// 	$debugScope = $('[data-ng-controller=ListsController]').scope();
// },1000);
