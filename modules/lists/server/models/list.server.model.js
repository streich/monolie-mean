'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * List Schema
 */
var ListSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please enter name',
		trim: true
	},
	description: {
		type: String,
		default: ''
	},
	share: {
		type: Boolean,
		default: ''
	},
	height: {
		type: Number,
		default: 40
	},
	favorite: {
		type: Boolean,
		default: ''
	},
	block: {
		type: Boolean,
		default: false
	},
	locked: {
		type: Number,
		default: 0
	},

	mode: {
		type: Number,
		default: 0
	},

	itemIndex: {
		type: Number,
		default: 0
	},

	count: {
		type: Number,
		default: 0
	},
	created: {
		type: Date,
		default: Date.now
	},
	updated: {
		type: Date,
		default: Date.now
	},
	items: {
		type: Array,
		default: []
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	category: {
		type: Schema.ObjectId,
		ref: 'Category'
	}
});

mongoose.model('List', ListSchema);
