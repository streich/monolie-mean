'use strict';

/**
 * Module dependencies.
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Articles Permissions
 */
exports.invokeRolesPolicies = function() {
	acl.allow([{
		roles: ['admin'],
		allows: [{
			resources: '/api/lists',
			permissions: '*'
		}, {
			resources: '/api/favorites',
			permissions: '*'
		}, {
			resources: '/api/recent',
			permissions: '*'
		}, {
			resources: '/api/lists/:listId',
			permissions: '*'
		}, {
			resources: '/api/lists/categories/:catId',
			permissions: '*'
		}

		]
	}, {
		roles: ['user'],
		allows: [{
			resources: '/api/lists',
			permissions: ['get', 'post','delete','put']
		},
		{
			resources: '/api/collection',
			permissions: ['get','post']
		},

		{
			resources: '/api/lists/:listId',
			permissions: ['get','put','post','delete']
		}, {
			resources: '/api/favorites',
			permissions: ['get','put','post','delete']
		},
		{
			resources: '/api/recent',
			permissions: ['get','put','post','delete']
		},
		 {
			resources: '/api/lists/categories/:catId',
			permissions: ['get','put','post','delete']
		}]
	}, {
		roles: ['guest'],
		allows: [{
			resources: '/api/lists',
			permissions: ['get']
		}, {
			resources: '/api/lists/:listId',
			permissions: ['get']
		}]
	}]);
};

/**
 * Check If Articles Policy Allows
 */
exports.isAllowed = function(req, res, next) {
	var roles = (req.user) ? req.user.roles : ['guest'];

	// If an list is being processed and the current user created it then allow any manipulation
	if (req.list && req.user && req.list.user.id === req.user.id) {
		return next();
	}

	// Check for user roles
	acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function(err, isAllowed) {
		if (err) {
			// An authorization error occurred.
			return res.status(500).send('Unexpected authorization error');
		} else {
			if (isAllowed) {
				// Access granted! Invoke next middleware
				return next();
			} else {
				return res.status(403).json({
					message: 'User is not authorized'
				});
			}
		}
	});
};
