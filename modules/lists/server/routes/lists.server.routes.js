'use strict';

/**
 * Module dependencies.
 */
var listsPolicy = require('../policies/lists.server.policy'),
	lists = require('../controllers/lists.server.controller');

module.exports = function(app) {
	// Articles collection routes
	app.route('/api/lists').all(listsPolicy.isAllowed)
		.get(lists.list)
		.post(lists.create);

	app.route('/api/collection').all(listsPolicy.isAllowed)
		.get(lists.exportAll)
		.post(lists.createMultiple);

	// Single list routes
	app.route('/api/lists/:listId').all(listsPolicy.isAllowed)
		.get(lists.read)
		.put(lists.update)
		.delete(lists.delete);


	app.route('/api/favorites').all(listsPolicy.isAllowed)
		.get(lists.listFavorites)
		.post(lists.create);


	app.route('/api/recent').all(listsPolicy.isAllowed)
		.get(lists.listRecent)
		.post(lists.create);
	// Lists Routes
	// app.route('/lists/public').all(listsPolicy.isAllowed)
	// 	.get(lists.listPublic);

	app.route('/api/lists/categories/:catId').all(listsPolicy.isAllowed)
		.get(lists.read)
		.put(lists.update)
		.delete(lists.delete);

	// Finish by binding the list middleware
	app.param('listId', lists.listByID);
	app.param('catId', lists.listByCategory);
};






// module.exports = function(app) {
// 	var users = require('../../app/controllers/users');
// 	var lists = require('../../app/controllers/lists');

// 	// Lists Routes
// 	app.route('/lists')
// 		.get(users.requiresLogin, lists.list)
// 		.post(users.requiresLogin, lists.create);

// 	app.route('/lists/favorites')
// 		.get(users.requiresLogin, lists.listFavorites)
// 		.post(users.requiresLogin, lists.create);

// 	app.route('/lists/categories/:catId')
// 		.get(users.requiresLogin, lists.read)
// 		.delete(users.requiresLogin, lists.hasAuthorization, lists.delete);


// 	app.route('/lists/:listId')
// 		.get(users.requiresLogin, lists.read)
// 		.put(users.requiresLogin, lists.hasAuthorization, lists.update)
// 		.delete(users.requiresLogin, lists.hasAuthorization, lists.delete);

// 	// Finish by binding the List middleware
// 	app.param('listId', lists.listByID);
// 	app.param('catId', lists.listByCategory);
// };
