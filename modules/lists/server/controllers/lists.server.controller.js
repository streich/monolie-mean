'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	List = mongoose.model('List'),
	_ = require('lodash');

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'List already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

/**
 * Create a List
 */
exports.create = function(req, res) {
	//@Todo  find abetter way. withouth this a "BAD REQUEST" error gets thrown on update
	if(req.body.category) req.body.category  = req.body.category._id;

	var list = new List(req.body);
	list.user = req.user;



	list.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(list);
		}
	});
};




/**
 * Create a List
 */
exports.createMultiple = function(req, res) {
	req.body.lists.forEach(function(listItem){
		if(listItem.category === false)  delete listItem.category;
		var list = new List(listItem);
		list.user = req.user;

		list.save(function(err) {
			// if (err) {
			// 	return res.send(400, {
			// 		message: getErrorMessage(err)
			// 	});
			// } else {
			// 	res.jsonp(list);
			// }
		});

	});

};



/**
 * Show the current List
 */
exports.read = function(req, res) {
	res.jsonp(req.list);
};

/**
 * Update a List
 */
exports.update = function(req, res) {
	var list = req.list ;

	//@Todo  find abetter way. withouth this a "BAD REQUEST" error gets thrown on update
	if(list.category) list.category  = list.category._id;


	list = _.extend(list , req.body);
	list.updated  = new Date();

	list.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(list);
		}
	});
};

/**
 * Delete an List
 */
exports.delete = function(req, res) {
	var list = req.list ;

	list.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(list);
		}
	});
};



/**
 * List of Lists
 */
exports.list = function(req, res) {
	List.find().where('user',req.user).select('-items').sort('-updated').populate('category','name marker').populate('user', 'displayName').exec(function(err, lists) {

		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(lists);
		}
	});
};


exports.exportAll = function(req, res) {
	List.find().where('user',req.user).populate('category','name marker').select({user:0}).exec(function(err, lists) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(lists);
		}
	});
};



exports.listRecent = function(req, res) {
	List.find().where('user',req.user).select('-items -created -share').sort({updated: -1}).populate('category','name marker').exec(function(err, lists) {

		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(lists);
		}
	});
};
/**
 * List middleware
 */
exports.listByID = function(req, res, next, id) {
	 List.findById(id).where('user',req.user).populate('category','name marker').populate('user', 'displayName').exec(function(err, list) {
		if (err) return next(err);
		if (!list) return next(new Error('Failed to load List ' + id));
		req.list = list ;
		next();
	});
};



/**
 * List all favorites (not tested yet)
 */
exports.listFavorites = function(req, res) {
	// List.find().sort('-updated').where('user',req.user).populate('category','name').populate('user', 'displayName').exec(function(err, lists) {
	List.find().where('user',req.user).where('favorite',true).select('-items').sort('-created').populate('category','name marker').populate('user', 'displayName').exec(function(err, lists) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			// console.log(lists);
			res.jsonp(lists);
		}
	});
};



/**
 * List middleware  (not tested yet)
 */
exports.listByCategory = function(req, res, next, id) {
	List.find().where('user',req.user).where('category',id).select('-items').populate('category','name marker').populate('user', 'displayName').exec(function(err, lists) {
	if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			// console.log(lists);
			res.jsonp(lists);
		}
	});
};

/**
 * List authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.list.user.id !== req.user.id) {
		return res.send(403, 'User is not authorized');
	}
	next();
};
