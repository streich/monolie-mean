'use strict';
/*global $:false */
/*global _:false */

// Categories controller
angular.module('categories').controller('CategoriesController', ['$log','$scope','$rootScope','$stateParams','$state', '$location', 'Authentication', 'Categories','modalServiceCategories','CATEGORY_ICONS',
	function($log,$scope, $rootScope, $stateParams, $state, $location, Authentication, Categories,modalServiceCategories,CATEGORY_ICONS ) {
		$scope.authentication = Authentication;
		if(!$rootScope.categories) $rootScope.categories  =[];
		$scope.$rootScope = $rootScope;
				$scope.CATEGORY_ICONS = CATEGORY_ICONS;

		$scope.markers = [
			{id:0,name:'0'},
			{id:1,name:'1'},
			{id:3,name:'3'},
			{id:4,name:'4'},
			{id:5,name:'5'},
			{id:6,name:'6'},
			{id:7,name:'7'},
			{id:8,name:'8'},
			{id:9,name:'9'}
		];

		// Create new Category
		$scope.create = function() {
			// Create new Category object
			var category = new Categories ({
				name: this.name,
				marker: this.marker
			});

			// Redirect after save
			category.$save(function(response) {
				$location.path('categories/' + response._id);
				// $location.path('categories/' + response._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			// update scope
			$scope.find();

			// Clear form fields
			this.name = '';
			this.marker = 0;
		};


		// Create new Category
		$scope.createNew = function(name,marker) {
			// Create new Category object
			var category = new Categories ({
				name: name,
				marker: marker
			});

			// Save Category
			category.$save(function(response) {
				$scope.find();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Remove existing Category
		$scope.remove = function( category ) {
			$log.log('remove',category);
			if ( category ) {

				category.$remove(function() {
					$scope.find();
					$location.path('/');
				});

			} else {
				$scope.category.$remove(function() {
				$scope.find();
				});
			}
		};

		// used by lists in category
		$scope.goToCategory = function(catId) {
				$rootScope.$broadcast('leaveList');
				var state = $state.current.name;
				$state.go('listListsByCategoryDetail', {
						catId: catId,
						listId: ''
				}, {
						notify: true
				});
				// $scope.find();
		};


		// Update existing Category
		$scope.update = function() {
			var category = $scope.category;
			category.$update(function() {
				$location.path('categories/' + category._id);
				// $location.path('categories/');
				$scope.find();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$rootScope.saveCategory = function(category) {
			if(!category || _.isUndefined(category)) return false;
			// $log.log('$rootScope.saveCategory', category);
			category.$update(function() {
			}, function(errorResponse) {
				// $scope.error = errorResponse.data.message;
			});

		};

		// Find a list of Categories
		$scope.find = function() {
		  Categories.query(function(response){
		  		// $log.log('$rootScope',$rootScope);
		  		if($rootScope){
						$rootScope.categories = response;
						$rootScope.categoriesLoaded = true;
					}
			});
		};

		// Find existing Category
		$scope.findOne = function() {
			$log.log('findOne',$stateParams);
			$scope.category = Categories.get({
				categoryId: $stateParams.catId
			});
		};

		$scope.openCreateCategoryModal = function() {
		   	modalServiceCategories.showModalCategoryCreate($scope).then(function(newCategory) {

		   		if(newCategory){
		   			$scope.createNew(newCategory.name,newCategory.marker);
		   		}
		    });
	  	};

	  	$scope.openCategoryOptionsModal = function(category) {
		   	modalServiceCategories.showModalCategoryOptions(category,$scope).then(function(category) {
		   		if(category){
		   			$scope.category = category;
		   			$scope.category.$update();
		   		}
		    });
	  	};
	}

]);
