'use strict';

//modal Service
angular.module('categories').service('modalServiceCategories', ['$modal',
    function ($modal) {


       this.showModalCategoryCreate = function (parentScope) {
             console.log('showModalCreateCategory');
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/categories/views/modal-category-create.client.view.html',
                  controller: function ($scope, $modalInstance) {
                      $scope.parentScope = parentScope;
                      $scope.category = {name:'name', marker:0};


                      $scope.setMarker = function (marker) {
                        $scope.category.marker = marker;
                      };

                      $scope.ok = function () {
                        console.log('ok',$scope.category);
                        $modalInstance.close($scope.category);
                      };

                      $scope.cancel = function () {
                        $modalInstance.close(false);
                      };

                    }
                }).result;
            };

           this.showModalCategoryOptions = function (category,parentScope) {
             return $modal.open({
                size: 'md',
                  templateUrl: 'modules/categories/views/modal-category-options.client.view.html',
                  controller: function ($scope, $modalInstance) {

                      $scope.category = category;
                      $scope.parentScope = parentScope;


                      $scope.removeCategory = function () {
                        parentScope.remove(category);
                         $scope.cancel();
                      };
                      $scope.ok = function () {
                        $modalInstance.close($scope.category);
                      };
                      $scope.cancel = function () {
                        $modalInstance.close();
                      };
                    }
                }).result;
            };



    }]);
