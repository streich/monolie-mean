'use strict';

/**
 * Module dependencies.
 */
var categoriesPolicy = require('../policies/categories.server.policy'),
	categories = require('../controllers/categories.server.controller');

module.exports = function(app) {
	// Articles collection routes
	app.route('/api/categories').all(categoriesPolicy.isAllowed)
		.get(categories.list)
		.post(categories.createCategory);
	app.route('/api/categories/collection').all(categoriesPolicy.isAllowed)
		.post(categories.createCategories)
		.get(categories.exportAll);

	// Single article routes
	app.route('/api/categories/:categoryId').all(categoriesPolicy.isAllowed)
		.get(categories.read)
		.put(categories.update)
		.delete(categories.delete);

	// Finish by binding the article middleware
	app.param('categoryId', categories.categoryByID);
};
