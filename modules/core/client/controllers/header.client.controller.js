'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', '$stateParams', 'Authentication', '$localStorage',
    function($scope, $state, $stateParams, Authentication, $localStorage) {
        // Expose view variables
        $scope.$state = $state;
        $scope.$stateParams = $stateParams;
        $scope.authentication = Authentication;
        $scope.$storage = $localStorage;
        // Get the topbar menu
        // $scope.menu = Menus.getMenu('topbar');

        // Toggle the menu items
        $scope.isCollapsed = false;
        $scope.tooltipDelay = 0;

        function _updateTooltipDelay() {
            if ($scope.$storage.miniMode) {
                $scope.tooltipDelay = 0;
            } else {
                $scope.tooltipDelay = 50000;

            }
        }
        _updateTooltipDelay();
        $scope.toggleLayoutMode = function() {
            console.log($scope.$storage);
            $scope.$storage.miniMode = !$scope.$storage.miniMode;
            _updateTooltipDelay();

        };



        $scope.toggleCollapsibleMenu = function() {
            $scope.isCollapsed = !$scope.isCollapsed;
        };

        // Collapsing the menu after navigation
        $scope.$on('$stateChangeSuccess', function() {
            $scope.isCollapsed = false;
        });


    }
]);