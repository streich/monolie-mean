'use strict';

// angular.module('core').run(function($templateCache) {
//   $templateCache.get('/modules/lists/client/views/modal-item-options.html');
//   $templateCache.get('/modules/lists/client/views/modal-list-settings.html');
//   $templateCache.get('/modules/lists/client/views/modal-list-categories.html');
// });

angular.module('core').controller('CoreController', ['$scope','$rootScope','$location', 'Authentication','$localStorage',
	function($scope,$rootScope,$location, Authentication,$localStorage) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.$storage = $localStorage;
		$scope.$rootScope = $rootScope;

		// if($scope.$storage.miniMode) $scope.$storage.miniMode = false;

		if(!$scope.authentication.user){
			$location.path('/authentication/signin');
		}
	}
]);
