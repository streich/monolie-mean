'use strict';
/*global $:false */
/*global _:false */



angular.module('core').directive('slideable', function() {
    return {
        restrict: 'C',
        compile: function(element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '0.3s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
}).directive('slideHide', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var target = $(attrs.slideHide);
            attrs.expanded = false;
            $(target).attr('expanded', 'false');

            element.bind('click', function() {
                var content =  $(target).find('.slideable_content');
                target.height(0);
                $(target).attr('expanded', 'false');
                attrs.expanded = !attrs.expanded;
            });
        }
    };
}).directive('slideToggle', ['$rootScope','$timeout', function($rootScope,$timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var target = document.querySelector(attrs.slideToggle);
            attrs.expanded = false;
            $(target).attr('expanded', 'false');

            function _refreshHeight(force){
                $timeout(function(){
                    if(force === true || $(target).attr('expanded') === 'true'){
                        var content = target.querySelector('.slideable_content');
                        content.style.border = '1px solid rgba(0,0,0,0)';
                        var y = content.clientHeight;
                        content.style.border = 0;
                        target.style.height = y + 'px';
                    }
                },30);
            }

            $rootScope.$on('error',_refreshHeight);
            $rootScope.$on('success',_refreshHeight);

            element.bind('click', function() {
                if ($(target).attr('expanded') === 'false') {
                    $(element).addClass('slideable-collapsed');
                   _refreshHeight(true);
                    $(target).attr('expanded', 'true');
                } else {
                    $(element).removeClass('slideable-collapsed');
                    target.style.height = '0px';
                    $(target).attr('expanded', 'false');
                }
                attrs.expanded = !attrs.expanded;
            });
        }
    };

}]).directive('slideToggleSub', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var target = document.querySelector(attrs.slideToggleSub);
            element.bind('click', function() {
                $(target).trigger('click');
            });
        }
    };
})
.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
      });
    }
  };
}]);
