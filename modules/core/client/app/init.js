'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.html5Mode(true).hashPrefix('!');
	}
])
.constant('CATEGORY_ICONS', ['none','bear','birthday','book','brain','bug','bulb','calendar','car','cart','checkbox','code','computer','cooking','female','fox','heart','list','mail','male','music','newspaper','people','phone','place','question','quote','sheet','stock','suitcase','table','thought','video','world'])
;

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
