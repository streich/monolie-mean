'use strict';

// module.exports = {
// 	// Development assets
// };
module.exports = {
	client: {
		lib: {
			css: [
				'public/lib/angular-hotkeys/build/hotkeys.min.css',
				'public/lib/angular-ui-tree/dist/angular-ui-tree.min.css',
				'public/lib/angular-carousel/dist/angular-carousel.css'
				],
			js: 
				[
			

			],
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	}
};
