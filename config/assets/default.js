'use strict';

module.exports = {
	client: {
		lib: {
			css: [
				// 'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/angular-ui-tree/dist/angular-ui-tree.min.css',
				// 'public/lib/angular-loading-bar/build/loading-bar.min.css',
				'public/lib/angular-carousel/dist/angular-carousel.css'
				// 'public/dist/application.min.css'
				],
			js:
				[
			// 'http://code.jquery.com/jquery-2.1.1.min.js',
				'public/lib/jquery/dist/jquery.js',
				'public/lib/jquery-mousewheel/jquery.mousewheel.min.js',
				'public/lib/underscore-amd/underscore-min.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-cookies/angular-cookies.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-masonry/angular-masonry.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-loading-bar/build/loading-bar.min.js',
				'public/lib/angular-touch/angular-touch.js',  // used by angular-carousel
				'public/lib/smartdate/smartdate.js',
				'public/lib/marked/marked.min.js',
				'public/lib/rangy-official/rangy-core.js',
				'public/lib/file-saver.js/FileSaver.js',
				'public/lib/angular-filter/dist/angular-filter.js',
				'public/lib/keymaster/keymaster.js',
				'public/lib/angular-logex/dist/log-ex-unobtrusive.js',
				// 				'public/lib/angular-logX/release/amd/angular-logX.js',

				// 'public/lib/rangy-official/rangy-classapplier.js',
				// 'public/lib/undo/undo.js',
				// 'public/lib/medium.js/medium.js',

				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/angular-carousel/dist/angular-carousel.js',
				'public/lib/angular-elastic/elastic.js',
				'public/lib/angular-file-upload/angular-file-upload.js',
				'public/lib/ngstorage/ngStorage.js',
				'public/lib/angular-vs-repeat/src/angular-vs-repeat.js',
				'public/lib/ngInfiniteScroll/build/ng-infinite-scroll.min.js',
				'public/lib/angular-ui-tree/dist/angular-ui-tree.js'

			],
			tests: ['public/lib/angular-mocks/angular-mocks.js']
		},
		css: [
			// 'modules/*/client/css/*.css'
			'modules/core/client/css/core.css'

		],
		less: [
			'modules/*/client/less/*.less'
		],
		sass: [
			'modules/*/client/sass/*.sass'
		],
		sassWatch: [
			'modules/*/client/sass/**/*.sass'
		],

		js: [
			'modules/core/client/app/config.js',
			'modules/core/client/app/init.js',
			'modules/*/client/*.js',
			'modules/*/client/**/*.js'
		],
		views: ['modules/*/client/views/**/*.html']
	},
	server: {
		allJS: ['gruntfile.js', 'server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
		models: 'modules/*/server/models/**/*.js',
		routes: ['modules/*[!core]/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
		sockets: 'modules/*/server/sockets/**/*.js',
		config: 'modules/*/server/config/*.js',
		policies: 'modules/*/server/policies/*.js',
		views: 'modules/*/server/views/*.html'
	}
};
