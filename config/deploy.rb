# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'monolie_mean'
set :repo_url, 'git@bitbucket.org:streich/monolie-mean.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, 'master'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/opt/monolie2'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :user, "deploy"
set :use_sudo, true

# namespace :deploy do
#
#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end
#
# end


namespace :deploy do

	desc 'Install npm and bower dependencies'
	task :npm_bower do
		on roles(:app), in: :sequence do
			within release_path do
				execute "cd #{release_path}"
				execute :npm, 'install --production'
				# execute :bower, 'install --silent'
			end
		end
	end

	desc 'Build Assets'
	task :build_assets do
		on roles(:app), in: :sequence do
			within release_path do
				# execute "cd #{release_path}"
				# execute :gulp, 'build'
			end
		end
	end

	desc 'Restart application'
	task :restart do
		on roles(:app), in: :sequence, wait: 5 do
			within release_path do

					# restart.txt is used by passenger to restart the server
					# execute :mkdir, '-p tmp'
					# execute :touch, 'tmp/restart.txt'
					# execute "forever stopall"
					# execute "NODE_ENV=production forever start #{release_path.join('server.js')}"
					# execute "node stop #{release_path.join('server.js')}"
					# execute "NODE_ENV=production node start #{release_path.join('server.js')}"

					execute "pm2 kill"
					# execute "NODE_ENV=production pm2 start #{release_path.join('server.js')}"
					execute "cd #{release_path} && NODE_ENV=production pm2 start server.js"

					# TODO curl the shop url to reload the node process via passenger
			end
		end
	end

	after :updated, :npm_bower
	after :npm_bower, :build_assets
	after :publishing, :restart

end
