# Interesting Facts
- At 188 decibels, the Blue whale is the loudest mammal of all.
- The only state in the U.S that grows coffee beans is Hawaii.
- Honey is the only food that doesn’t spoil.
- The Oilbird (oil bird) is the loudest bird.
- According to a new study, next day soreness after hard work or exercising can be greatly reduced by drinking watermelon juice prior. Watermelon juice is rich in amino acid.
- It takes about 7 strawberries to equal the amount of vitamin C in one orange about 3 inches in diameter.
The very first food eaten by a U.S astronaut in outer space was applesauce.
- 53 Prozent Highschool Absolventen und 27 Prozent College Absolventen haben das meiste ihres Wissens aus dem Fernsehen.
- Statistisch gesehen ist das sicherste Alter 10 Jahre.
- Kaffee ist das zweitgrößte Produkt auf der Liste des Internationalen Verkaufs auf der Welt.

#Zitate
- I may be drunk, Miss, but in the morning I will be sober and you will still be ugly.
 - Winston Churchill
- If you're going through hell, keep going.
 - Winston Churchill
- Attitude is a little thing that makes a big difference.
 - Winston Churchill
- The best argument against democracy is a five-minute conversation with the average voter.
 - Winston Churchill
- The empires of the future are the empires of the mind.
 - Winston Churchill
- A joke is a very serious thing.
 - Winston Churchill
- A pessimist sees the difficulty in every opportunity; an optimist sees the opportunity in every difficulty.
 - Winston Churchill
- Habit, if not resisted, soon becomes necessity.
 - Augustine of Hippo
- Happiness is not something ready made. It comes from your own actions.
 - Dalai Lama
- My religion is very simple. My religion is kindness.
 - Dalai Lama
- "Bad taste creates many more millionaires than good taste."
 - ~ Charles Bukowski
- "If you want to know who your friends are, get yourself a jail sentence." ~ Charles -  - Bukowski
- Today's Problems cannot be solved if we still think the way we created them
 - Albert Einstein
category: Learning

# Zarathustra
- Es muss noch..

#Zitate Deutsch
- Mit kleinen Jungen und Journalisten soll man vorsichtig sein. Die schmeißen immer noch einen Stein hinterher.
 - Konrad Adenauer
- Machen Sie sich erst einmal unbeliebt, dann werden Sie auch ernst genommen.
 - Konrad Adenauer
- Man schafft niemals Veränderung, indem man das Bestehende bekämpft, um etwas zu verändern, baut man neue Modelle, die das Alte überflüssig machen.”
 - Buckminister Fuller

#A short history of nearly everything
-  All the visible stuff in our solar system including sun, planets and a billion asteroids fill up less than a trillionth of the available space.
- We all remember the diagram of our solar system from the classroom, but on a true scale diagram, if the earth were the size of a pea, Pluto would be a mile and a half away.
- The distance to the edge of our solar system is 50,000 times the distance to Pluto.
- Based on what we know, there is absolutely no prospect that any human will ever leave our solar system.
- Isaac newton would sometimes wake up in the morning and when swinging his feet out of bed  would sit frozen for hours by the rush of thoughts to his head.
- Newton’s Principia was only published thanks to Edmond Halley’s financing of it.
The royal society was having financial difficulties due to the fact that last year’s publication The History Of Fishes was a flop.
- The geologist and paleontologist William Buckland  was determined to taste every animal on the planet.
- Best remembered for coining the word Dinosaur, Richard Owen also gave us the modern concept of museums as places the common folk can visit and not just scientists.
He was also one of the meanest persons in science history and the only person Darwin ever hated.
And he looked like Ebeneezer Scrooge.
- Carl Wilhelm Scheele one of the founders of modern chemistry, had a habit of sniffing and tasting any new element or chemical he discovered including poisonous ones.
He was found dead at the age of 43, killed by his last discovery.
-  Shortly after publishing his landmark papers featuring E = mc^2 \,\!, Albert Einstein applied for the positions of university lecturer and high school teacher and was rejected in both cases.
-  For forty years, every study of the effects of lead exposure on humans was funded by a lead additive company, we still don’t know the damage sustained by the planet and mankind due to this crime.
-  People who are directly in a meteor’s path will not be killed by the impact.
The compressed air in the path will heat up to 60,000 degrees Celsius, making them instantly vanish.
-  Tokyo is referred to by geologists as “the city waiting to die”.
The book was published in 2003, before the Fukushima event.
-  In the early days of pump and hose assisted diving, there was a dreaded phenomena called “the squeeze” where the diver’s entire body would be sucked into the hose and diving helmet, leaving just some bones and flesh in the diving suit.
Ouch.
-  For dozens of years, nuclear waste was dumped in the oceans using hundreds of thousands of plastic drums that would sometimes be perforated with machine guns to help them sink.
And I thought Monty Burns was bad.
-  The fish used in fish sticks was originally cod, then haddock, then redfish and lately pacific pollock.
Fish is whatever’s left in the oceans.
- Slime molds are amazing creatures.
- The human body has about 10 million different varieties of white blood cells, each designed to destroy a specific enemy.
- We have fossil records of  only one species for  every 120,000 species who have lived on the planet.
- The Permian extinction killed off at least 95% of all the animals we know about from the fossil records.
- The energy released by the dinosaur killing meteorite was 100 million megatons or about one Hiroshima-sized bomb for every human on the planet.
- You care for nothing but shooting, dogs, and rat- catching, and you will be a disgrace to yourself and all your family.
Robert Darwin to his son Charles upon seeing his academic grades.
- Ironically, the one thing On the Origin of the Species didn’t explain was how species originated.
- A human body contains 20 million kilometers of coiled DNA.
- Every living thing is an elaboration on a single original plan.
All life is one.

category: Learning

description: Facts and citations form Bill Brysons Book "a short history of nearly everything"

created: 10.11.2013


# French Vocabulary (1)


category: Learning

mode: 0

accessed: 10.01.2013

created: 10.01.2013

favorite: true



# Who is this actor
- http://daedrianmcnaughton.com/wp-content/uploads/2012/12/Martin-Sheen.jpg
	- Martin Sheen
- http://www.moviepilot.de/files/images/0261/5164/Charlie_Sheen222.jpg
	- Charlie Sheen
- http://www.nndb.com/people/878/000025803/estevez2.jpg
	- Emilio Estevez
- https://si0.twimg.com/profile_images/1990249248/image.jpg
	- Will Ferrell

category: Fun

# Sprichwörter
- Das Gras wächst nicht schneller wenn man daran zieht. (Afrikanisches Sprichwort)
- Man muss das Eisen schmieden, solange es heiß ist.
- Neid ist die ehrlichste Form der Anerkennung
- Bescheidenheit ist die höchste Form der Arroganz
- Es ist nicht alles Gold, was glänzt.
- Ein Löffel voll Tat ist besser als ein Löffel voll Rat.
- Wer viel fordert, bekommt viel. Wer zu viel fordert, bekommt nichts.
- Eine Schwalbe macht noch keinen Sommer.
- Wer den Acker pflegt, den pflegt der Acker
- Ein guter Amboss fürchtet keinen Hammer
- Eine Hand wäscht die andere.
- Wer ein grosses Maul hat, muss einen breiten Rücken haben.
- Gehe dem Bach nach und du findest das Meer.
- Die Faulen und Dreisten schreien am meisten.
- Nicht jedes Feld trägt jede Frucht.
- Wer sich selber kennt, spottet nicht über andere.
- Wo Rauch ist, ist auch Feuer.
- Wer Wind sät, wird Sturm ernten.
- Der Esel nennt sich immer zuerst
- Besser eigenes Brot als fremder Braten.
- "Da liegt der Hase im Pfeffer."
- Anfangen ist leicht, beharren eine Kunst.
- Das Glück ist mit dem Tüchtigen.
- Lieber den Magen verrenkt,als dem Wirt was g'schenkt.
- Je fester man eine Brennesel anfasst, desto weniger brennt sie.
- Gut Ding will Weile haben.
- Erst, wenn der Brunnen trocken ist, schätzt man das Wasser.
- Der Apfel fällt nicht weit vom Stamm.
- Zwischen Leber und Milz passt immer ein Pils
- Steter Tropfen höhlt den Stein
***
source: http://de.wikiquote.org/wiki/Deutsche_Sprichw%C3%B6rter

category: Fun



# Fremdwörter
- alternieren
 - <lat.>: [ab]wechseln, einander ablösen
- arbiträr
 - <lat.-franz.> willkürlich
- Axiom
 - <gr.-lat.>: 1. als absolut richtig anerkannter Grundsatz; gültige Wahrheit, die keines Beweises bedarf. 2. nicht abgeleitete Aussage eines Wissenschaftsbereiches, aus der andere Aussagen deduziert werden.
- bilateral
 - <lat.-nlat.>: 1. zweiseitig; zwei Seiten, Partner betreffend; von zwei Seiten ausgehend. 2. (Biol.) bilateralsymmetrisch
- ceteris paribus
 - <lat.>: unter [sonst] gleichen Umständen
derivativ
(Sprachw.) durch Ableitung entstanden
- diffamieren
 - jmdn. in seinem Ansehen, etwas in seinem Wert herabsetzen, verunglimpfen; jmdn./etwas in Verruf bringen.
- disponieren
 - <lat.>: a) in bestimmter Weise verfügen; b) im Voraus [ein]planen, kalkulieren
- dogmatisch
 - (gr.-lat) starr an eine Ideologie od. Lehrmeinung gebunden bzw. daran festhaltend
- Dogma
 - <gr.-lat.>: fester, als Richtschnur geltender [religiöser, kirchlicher] Lehr-, Glaubenssatz
- Denunziant
- Verräter
- amortisieren
 - abzahlen
- hermetisch
 - dicht verschlossen, nichts kann ein- oder hinausdringen
- Gallert
 - gelatineartige Masse
- Divergenz
 - Abweichung, Meinungsverschiedenheit
- Exaltiertheit
 - hysterische Erregung, Überspanntheit
- Antagonismus
 - "Widerstreit; Gegensatz, der nicht überwunden werden kann (Meinungen)"
- marginal
 - am Rande liegend
- Divergenz
 - Abweichung, Meinungsverschiedenheit
- Hommage
 - Würdigung
- Altruismus
 - Uneigennützigkeit, selbstlose Denk u. Handlungsweise
- Inklusion
 - das Zusammenleben mit anderen Religionen, Kulturen...
- Manifestation
 - Sichtbarwerden oder Bekanntmachung
- profund
 - (all)umfassend
- transzendent
 - übersinnlich
- obligatorisch
 - durch ein Gesetz vorgeschrieben, verbindlich

category: Learning

# Fun Stuff
- Swaghetti Yolonese
- Yolo Swaggins

category: Fun

# Es reimt sich
- Mit Schwung, fit und Jung.
- jawohl tirol

category: Fun

# Bundesliga fakten
- Die 1. Bundesliga wurde erst am 28. Juli 1962 eingeführt.
- Die 2. Bundesliga gibt es erst seit 1974, während es zuvor 5 Regionalligen gab, welche zusammen die zweithöchste Liga ergaben.
- Vor der Gründung der Bundesliga, gab es 55 regionale Ligen in Deutschland.
- 1958 scheiterte die Gründung einer 1. Bundesliga, da die Verantwortlichen es nicht für möglich hielten, dass Fussball vermarktet werden kann. Sie rechneten damit, dass die Vereine in kurze Zeit pleite gehen würden. Dem ist nicht so, wie man später sah.
- Rekordmeister ist der FC Bayern München mit 21 Titeln seit Gründung der Bundesliga im Jahre 1962.
- Bis 1986 gab es noch keine Winterpause und der Spielbetrieb ging einfach weiter, egal ob Weihnachten oder Neujahr.
- So fand 1964 ein Spiel, zwischen dem 1. FC Nürnberg und VfB Stuttgart, am Sylvestertag statt (31. Dezember).
- Der Vorschlag für den jährlichen Spielplan der 1. Bundesliga, wird von einem Computerprogramm gemacht. Dabei werden erst diverse Grossanlässe und Feiertage definiert und los geht es.
- Der Spielplan muss von der Zentralen Informationsstelle für Sicherheit (ZIS) abgesegnet werden.
- Drei Punkte für einen Sieg gibt es erst seit 1995.
- Bisher hat erst ein Verein auf Grund von finanziellen Problemen keine Lizenz für die 1. Bundesliga bekommen (Dynamo Dresden).
- Zu Verstössen gegen Lizenzauflagen kommt es leider immer wieder. Diese werden mit Geldstrafen oder Punkteabzügen bestraft.
- Borussia Mönchen Gladbach und der FC Bayern München sind die einzigen Vereine, welche ein Meisterschafts-Hattrick erreichten (Bayern sogar deren drei).
- Der Wettskandal im Jahre 2005, war bereits der zweite grosse Skandal in der Bundesligageschichte. So kam es in der Saison 70/71 zu Manipulationen von mehreren Spielen, in deren Folgen Arminia Bielefeld und die Kickers Offenbach nicht abstiegen. Der Schwindel fiel aber auf und beiden wurde die Lizenz entzogen. Heute gibt es in der Bundesliga eine ganze Reihe an Massnahmen, welche verhindern, dass zum Beispiel ein Schiedsrichter ein Spiel manipulieren kann. Deshalb geniesst das Wetten im Fussball heute wieder einen guten Ruf.
- Karl-Heinz Rummenigge war der erste deutsche Spieler, welche für eine Ablösesumme von mehr als 10 Mio. Mark verkauft wurde (von FC Bayern München zu Inter Mailand).
- Der 1. FC Nürnberg und Arminia Arminia Bielefeld sind je 7 mal auf- und abgestiegen, das ist Rekord.
- Der Signal Iduna Park ist das grösste Stadion der Bundesliga. Es fasst 80'720 Zuschauer.
- Mit 99,4% geniesst der FC Schalke 04 die beste Auslastung des eigenen Stadion.
- Seit 1985 sind die Lizenzkosten für die Übertragungsrechte der 1. Bundesliga von nahezu Null auf über 400 Mio. Euro gestiegen.
- Sepp Maier schaffte es 442 Spiele in Folge zu bestreiten. Diese Serie wurde zudem nur auf Grund eines Autounfalls unterbrochen.


# What is the Capital of..?
- Afghanistan
	- Kabul
- Albania
	- Tirana
- Algeria
	- Algiers
- Andorra
	- Andorra la Vella
- Angola
	- Luanda
- Antigua and Barbuda
	- Saint John's
- Argentina
	- Buenos Aires
- Armenia
	- Yerevan
- Australia
	- Canberra
- Austria
	- Vienna
- Azerbaijan
	- Baku
- The Bahamas
	- Nassau
- Bahrain
	- Manama
- Bangladesh
	- Dhaka
- Barbados
	- Bridgetown
- Belarus
	- Minsk
- Belgium
	- Brussels
- Belize
	- Belmopan
- Benin
	- Porto-Novo
- Bhutan
	- Thimphu
- Bolivia
	- La Paz (administrative); Sucre (judicial)
- Bosnia and Herzegovina
	- Sarajevo
- Botswana
	- Gaborone
- Brazil
	- Brasilia
- Brunei
	- Bandar Seri Begawan
- Bulgaria
	- Sofia
- Burkina Faso
	- Ouagadougou
- Burundi
	- Bujumbura
- Cambodia
	- Phnom Penh
- Cameroon
	- Yaounde
- Canada
	- Ottawa
- Cape Verde
	- Praia
- Central African Republic
	- Bangui
- Chad
	- N'Djamena
- Chile
	- Santiago
- China
	- Beijing
- Colombia
	- Bogota
- Comoros
	- Moroni
- Congo, Republic of the
	- Brazzaville
- Congo, Democratic Republic of the
	- Kinshasa
- Costa Rica
	- San Jose
- Cote d'Ivoire
	- Yamoussoukro (official); Abidjan (de facto)
- Croatia
	- Zagreb
- Cuba
	- Havana
- Cyprus
	- Nicosia
- Czech Republic
	- Prague
- Denmark
	- Copenhagen
- Djibouti
	- Djibouti
- Dominica
	- Roseau
- Dominican Republic
	- Santo Domingo
- East Timor (Timor-Leste)
	- Dili
- Ecuador
	- Quito
- Egypt
	- Cairo
- El Salvador
	- San Salvador
- Equatorial Guinea
	- Malabo
- Eritrea
	- Asmara
- Estonia
	- Tallinn
- Ethiopia
	- Addis Ababa
- Fiji
	- Suva
- Finland
	- Helsinki
- France
	- Paris
- Gabon
	- Libreville
- The Gambia
	- Banjul
- Georgia
	- Tbilisi
- Germany
	- Berlin
- Ghana
	- Accra
- Greece
	- Athens
- Grenada
	- Saint George's
- Guatemala
	- Guatemala City
- Guinea
	- Conakry
- Guinea-Bissau
	- Bissau
- Guyana
	- Georgetown
- Haiti
	- Port-au-Prince
- Honduras
	- Tegucigalpa
- Hungary
	- Budapest
- Iceland
	- Reykjavik
- India
	- New Delhi
- Indonesia
	- Jakarta
- Iran
	- Tehran
- Iraq
	- Baghdad
- Ireland
	- Dublin
- Israel
	- Jerusalem*
- Italy
	- Rome
- Jamaica
	- Kingston
- Japan
	- Tokyo
- Jordan
	- Amman
- Kazakhstan
	- Astana
- Kenya
	- Nairobi
- Kiribati
	- Tarawa Atoll
- Korea, North
	- Pyongyang
- Korea, South
	- Seoul
- Kosovo
	- Pristina
- Kuwait
	- Kuwait City
- Kyrgyzstan
	- Bishkek
- Laos
	- Vientiane
- Latvia
	- Riga
- Lebanon
	- Beirut
- Lesotho
	- Maseru
- Liberia
	- Monrovia
- Libya
	- Tripoli
- Liechtenstein
	- Vaduz
- Lithuania
	- Vilnius
- Luxembourg
	- Luxembourg
- Macedonia
	- Skopje
- Madagascar
	- Antananarivo
- Malawi
	- Lilongwe
- Malaysia
	- Kuala Lumpur
- Maldives
	- Male
- Mali
	- Bamako
- Malta
	- Valletta
- Marshall Islands
	- Majuro
- Mauritania
	- Nouakchott
- Mauritius
	- Port Louis
- Mexico
	- Mexico City
- Micronesia, Federated States of
	- Palikir
- Moldova
	- Chisinau
- Monaco
	- Monaco
- Mongolia
	- Ulaanbaatar
- Montenegro
	- Podgorica
- Morocco
	- Rabat
- Mozambique
	- Maputo
- Myanmar (Burma)
	- Rangoon (Yangon); Naypyidaw or Nay Pyi Taw (administrative)
- Namibia
	- Windhoek
- Nauru
	- no official capital; government offices in Yaren District
- Nepal
	- Kathmandu
- Netherlands
	- Amsterdam; The Hague (seat of government)
- New Zealand
	- Wellington
- Nicaragua
	- Managua
- Niger
	- Niamey
- Nigeria
	- Abuja
- Norway
	- Oslo
- Oman
	- Muscat
- Pakistan
	- Islamabad
- Palau
	- Melekeok
- Panama
	- Panama City
- Papua New Guinea
	- Port Moresby
- Paraguay
	- Asuncion
- Peru
	- Lima
- Philippines
	- Manila
- Poland
	- Warsaw
- Portugal
	- Lisbon
- Qatar
	- Doha
- Romania
	- Bucharest
- Russia
	- Moscow
- Rwanda
	- Kigali
- Saint Kitts and Nevis
	- Basseterre
- Saint Lucia
	- Castries
- Saint Vincent and the Grenadines
	- Kingstown
- Samoa
	- Apia
- San Marino
	- San Marino
- Sao Tome and Principe
	- Sao Tome
- Saudi Arabia
	- Riyadh
- Senegal
	- Dakar
- Serbia
	- Belgrade
- Seychelles
	- Victoria
- Sierra Leone
	- Freetown
- Singapore
	- Singapore
- Slovakia
	- Bratislava
- Slovenia
	- Ljubljana
- Solomon Islands
	- Honiara
- Somalia
	- Mogadishu
- South Africa
	- Pretoria (administrative); Cape Town (legislative); Bloemfontein (judiciary)
- South Sudan
	- Juba (Relocating to Ramciel)
- Spain
	- Madrid
- Sri Lanka
	- Colombo; Sri Jayewardenepura Kotte (legislative)
- Sudan
	- Khartoum
- Suriname
	- Paramaribo
- Swaziland
	- Mbabane
- Sweden
	- Stockholm
- Switzerland
	- Bern
- Syria
	- Damascus
- Taiwan
	- Taipei
- Tajikistan
	- Dushanbe
- Tanzania
	- Dar es Salaam; Dodoma (legislative)
- Thailand
	- Bangkok
- Togo
	- Lome
- Tonga
	- Nuku'alofa
- Trinidad and Tobago
	- Port-of-Spain
- Tunisia
	- Tunis
- Turkey
	- Ankara
- Turkmenistan
	- Ashgabat
- Tuvalu
	- Vaiaku village, Funafuti province
- Uganda
	- Kampala
- Ukraine
	- Kyiv
- United Arab Emirates
	- Abu Dhabi
- United Kingdom
	- London
- United States of America
	- Washington D.C.
- Uruguay
	- Montevideo
- Uzbekistan
	- Tashkent
- Vanuatu
	- Port-Vila
- Vatican City (Holy See)
	- Vatican City
- Venezuela
	- Caracas
- Vietnam
	- Hanoi
- Yemen
	- Sanaa
- Zambia
	- Lusaka
- Zimbabwe
	- Harare


# Vegan Facts

- Several studies show that a plant-based diet increases the body’s metabolism, causing the body to burn calories up to 16% faster than the body would on a meat-based diet for at least the first 3 hours after meals.j
- A number of researchers argue that while the human body is capable of digesting meat, our bodies are actually designed to be herbivores. For example, the human molars are similar to those of an herbivore, flat and blunt, which make them good for grinding, not gnashing and tearing.k
- Vegetarianism in India India has more vegetarians than any other country in the world
- Vegetarianism has roots in ancient India. In fact, currently 70% of the world’s vegetarians are Indians and there are more vegetarians in India than in any other country in the world.l
- The first Vegetarian Society was formed in England in 1847. The society’s goal was to teach people that it is possible to be healthy without eating meat.k
- In 2012, the Los Angeles city council unanimously approved a resolution that all Mondays in the City of Angels will be meatless. The measure is part of an international campaign to reduce the consumption of meat for health and environmental reason.g
- Several researchers argue that a vegetarian diet can feed more people than a meat-based diet. For example, approximately 20,000 pounds of potatoes can be grown on one acre of land. Comparatively, only around 165 pounds of beef can be produced on 1 acre of land.i
- There are several types of vegetarians. The strictest type is vegans. Vegans avoid not only meat but also all animal products. There is a debate within the vegan community about whether honey is appropriate for a vegan diet. For example, the Vegan Society and the American Vegan Society do not consider honey appropriate because it comes from an animal.j
- Studies show that a vegetarian diet could feed more people than a meat-based diet. For example, only around 20% of the corn grown in the United States is eaten by people, with about 80% of the corn eaten by livestock. Additionally, approximately 95% of the oats grown in the U.S are eaten by livestock. Studies show that the number of people who could be fed by the grain and soybeans that are currently fed to U.S. livestock is approximate 1,300,000,000.i
- ?A fruitarian is a type of vegetarian in which a person eats just fruits, nuts, seeds, and other plant material that can be harvested without killing the plant.j
- The total production of excrement by the U.S. population is 12,000 pounds per second. The total production of excrement by U.S. livestock is 250,000 pounds per second, which would be greatly reduced if humans ate a more plant-based diet and had little or no need for domesticated livestock. Less livestock would also greatly reduce Earth’s trapped greenhouse gases.i,e
- cows The grain fed to the 7 billion livestock in the U.S. could feed 800 million people
- Approximately 25 gallons of water are needed to produce 1 pound of wheat. Around 2,500 gallons of water are needed to produce 1 pound of meat. Many vegetarians argue that more people eating a meat-free diet would lower the strain that meat production puts on the environment.i
- A British study revealed that a child’s IQ could help predict his or her chance for becoming a vegetarian. The higher the IQ, the more likely the child will become a vegetarian.j
- Research reveals that if a man avoids red meats, it improves the sex appeal of his body odor.j
- Famous vegetarians include Leonardo da Vinci, Henry Ford, Brad Pitt, Albert Einstein, Ozzy Osborne, and (debatably) Hitler.k
- The American Dietetic Association (ADA) concludes that a vegetarian or vegan diet is healthier than one that includes meat. They note that vegetarians have lower body mass indices, lower rates of death from ischemic heart disease, lower blood cholesterol levels, lower blood pressure, lower rates of hypertension, type 2 diabetes, and less prostate and colon cancer.b
- Plants yield 10 times more protein per acre than meat.a
- Many vegetarians avoid meat because they ethically object to animal cruelty. For example, when stunners aren’t effective on hogs, they are sometimes sent to the scalding tanks, meant to soften the skin of dead pigs, while they are still alive and conscious.b
- Vegan diet A variety of plant sources can provide a vegetarian/vegan diet with enough protein
- Vegetarians have only slightly lower protein intake than those with a meat diet. Various studies around the world confirm that vegetarian diets provide enough protein if they include a variety of plant sources.j
- Vegetarians often chose to avoid meat based on ethical objections against animal cruelty. For example, poultry is not included in the Human Slaughter Act, so it is not required to stun them before they are shackled on a moving rail to have their throats slit. Some are still alive when they are submerged in the scalding tank. Those that are still alive are called “redskins.”b
- People become vegetarians for several reason, including ethical, health, political, environmental, cultural, aesthetic, and economic concerns.l
- An ovo-vegetarian will eat eggs but not other dairy products.j
- A lacto-vegetarian will eat dairy products but not eggs.j
- An ovo-lacto vegetarian diet includes both eggs and diary products.j
- Some vegetarians may not know that rennet is often used to make cheese and, therefore, unknowingly eat it. Rennet is extracted from the inner mucosa of the fourth stomach chamber of slaughtered young, unweaned calves.j
- Ironically, the original actor who played Ronald McDonald, Jeff Juliano, is now a vegetarian.a
- The number of animals killed for meat every hour in the U.S. is 500,000.a
- B12 deficiency Vitamin B12 is one of the few nutrients that comes only from animal sources
- Vegetarians can be deficient in Vitamin B12, which only comes from animal sources (though it can also be in fortified yeast extract products). Research suggests that a Vitamin B12 deficiency may be tied to the weakening of bones.j
- A pescetarian is a vegetarian who eats fish. Similar to a vegetarian diet, a pescetarian diet includes vegetables, fruits, grains, dairy, beans, and eggs. Unlike a vegetarian diet, a pescetarian diet also includes fish and shell fish. The term first originated in 1993 and is a blend of the Italian word pesce (fish) and the English word vegetarian.j
- Vegetarianism is based in the ancient Indian and Greek philosophies. India, vegetarianism was based on the philosophy of ahimsa or nonviolence toward animals. For the Hellenes and Egyptians, it had ritual or medical purposes. After Rome became Christianized, vegetarianism largely disappeared from Europe. It remerged in the Renaissance.k
- A Buddhist vegetarian (su vegetarianism) will not eat any animal products nor vegetables in the Allium family?such as onion, garlic, leeks, chives, and shallots?because the smell of these fetid vegetables is offensive and “angers up the blood.”l
- Jain vegetarians will eat dairy but not eggs, honey, or root vegetables.k
- The only vegetables with all eight types of essential amino acids in sufficient amounts are lupin beans, soy, hempseed, chia seed, amaranth, buckwheat, and quinoa. However, the essential amino acids can be achieved by eating other vegetables if they are in a variety.j
- Food, especially eating meat, has been a central question of Christian history. Many theologians argue that the vegetarian diet is the most compatible with Christian values, such as mercy and compassion.l
- China Study The China Study links eating animals to chronic disease
- The China Study was a 20-year study that compared the mortality rates of meat eaters and plant eaters. They found that countries that ate more animal-based food were more likely to have higher death rates from “Western diseases,” while countries that ate more plant food were healthier.j
- The China Study makes several arguments, including that a plant-based diet 1) plays a critical role in determining how genes are expressed, either good or bad; 2) controls the negative effects of unhealthy chemicals, 3) can help resolve chronic diseases, and 4) will create health in all areas of our lives. The China Study also argues that there are no nutrients in animal proteins that are better than plant-based proteins.j
- Ethical vegans are vegans who reject the commodity status of animals or animals that are used for shelter, food, or clothing.a
- The vegetarian movement has been influenced by ancient ethics of abstinence, early medical science that noted similarities between humans and animals, and Indian philosophy that promotes kindness to animals.k
- French philosopher Voltaire used the antiquity of Hinduism to launch a devastating blow to the Bible’s claims of dominance and acknowledged that the Hindus’ treatment of animals represented a “shaming alternative to the viciousness of European imperialists.”l
- One of the first famous vegetarians was the Greek philosopher Pythagoras who lived at the end of the 6th century B.C. In fact, the term “Pythagorean diet” was commonly used for a plant-based diet until the term “vegetarian” was coined in the 19th century.l
- Vegetarians such as the Manicheans and Cathars were considered heretics and persecuted during the medieval Inquisition.k,l
- The word “vegan” is derived from the word “vegetarian.” It was first used in 1944 when Elsie Shrigley and Donald Watson thought that the word “vegetarian” included too many types of animal by-products and did not encompass a completely plant-based diet.j
- An Oxford, England, study concluded that meat eaters were two and half times more likely to develop gallstones than non-meat eaters. Scientists concluded that the low-fat, high-fiber diet of vegetarians decreased the risk of developing gallstones.a
- Vegetarianism is still required for yogis in Hatha Yoga and Bhakti Yoga. Eating meat is said to lead to ignorance, sloth, and an undesirable mental state known as tamas. A vegetarian diet, on the other hand, leads to sattvic qualities that are associated with spiritual progress.l
- While vegetarian diets tend to be lower in calories and higher in fiber (which makes a person feel more full), some vegetarian diets can cause higher caloric intake than a meat diet if they include a lot of cheese and nuts.i
- A 2008 study by Time Magazine approximates the number of U.S. vegetarians at 7.3 million adults or 3.2% of the population. Of these, 0.5 % or 1 million are vegans.f
- Leonardo da Vinci Leonardo da Vinci argued that humans do not have a God-given right to eat animals
- The first Renaissance figure to advocate vegetarianism was Leonardo da Vinci. However, other influential figures, such as Immanuel Kant and Rene Descartes, did not believe humans had any ethical obligations toward animals.k
- Benjamin Franklin was an early American vegetarian (though he later became a meat-eater again). He introduced tofu to America in 1770.n
- Russian author Leo Tolstoy gave up meat because he was concerned about animal cruelty. He claims that eating meat is unnecessary, leads to animalistic feelings, excites human desires, and encourages “fornication and drunkenness.”n
- In the 20th century, English schoolmasters recommended that students become vegetarians as a way to curb their “appetites for self-abuse.”n
- A recent study argues that people who eat tofu and other plant-based foods have a better sex life than meat-eaters It claims that certain plants influence hormone levels and sexual activity.n
- According to one study, while women view vegetarian men as more principled, they also considered them “wimps” and “less macho” than those who eat meat.d
- While Hitler wasn’t willing to institute the policy during World War II, he did believe that vegetarianism could be key to Germany’s military success. He claimed that Caesar’s soldiers lived entirely on vegetables and the Vikings wouldn’t have been able to undertake their expeditions if they depended on a meat diet.k
- Several studies indicate that it would have been biologically impossible for humans to evolve large brains on a raw vegan diet. They conclude that meat-eating was crucial in human evolution.m
- A 2006 survey reveals that 6% of people in England are vegetarian, making the UK the European country with the largest proportion of its population that is vegetarian.f


# Redewendungen
- das Pferd beim Schwanz aufzäumen
- mit seinen Pfunden wuchern
- Die Rechnung ohne den Wirt machen
- Kommt Zeit, kommt Rat.

# Postillon
- Fassungslos: Glühbirne am Boden zerstört
- Computer gehackt: Holzfäller verliert sämtliche Daten
Neuer Internet Explorer 9 lädt Google Chrome oder Mozilla Firefox noch schneller herunter
- Gynäkologen warnen: Intimrasur führt meist zu Nachwuchs
- Heuschnupfen: Koksallergie zwingt Süchtigen zu Naturprodukten
- Einfach mal abschalten: Pfleger genießt Ruhe auf der Intensivstation
Nudelauflauf: Männliche Exhibitionisten treffen sich zum Essen
- Einbrecher stehlen Luftgitarre
- Minibarkeeper beinahe erfroren
- Yeti sichtet UFO
- Auskommen im Alter: Nachkommen müssen laut Abkommen vollkommen für Einkommen aufkommen
- Perverser Spanner von noch perverserem Spanner beobachtet

# Loriot

- Sie werden verstehen, dass unser Schützenpanzerwagen MS0872 auf dem Gabentisch – auch in netter Form – nicht gerne gesehen wird.“
 - Marzipankartoffel
- „Rein Produktionstechnisch besteht zwischen unserem Schützenpanzerwagen MS0872 und einer hoch qualifizierten Marzipankartoffeln kein nennenswerter Unterschied. Das ist letzten Endes nur eine Geschmacksfrage.“
„Dass die Alpen einen ganz erbärmlichen Anblick bieten, wenn man sich die Berge einmal wegdenkt?“
 - Wussten sie schon?
- „Dass der Walfisch das kleinste lebende Säugetier sein könnte, wenn er nur nicht so groß wäre.“
 - Wussten sie schon?
- „Dass alle deutschen Goldhamster aneinandergereiht von der Erde bis zum Mond reichen würden, wenn sie nur nicht so dumm wären.“
 - Wussten sie schon?
- „Krawehl, Krawehl! Taubtrüber Ginst am Musenhain. Trübtauber Hain am Musenginst. Krawehl, Krawehl!“ (Dichterlesung aus „Pappa ante Portas“)
- „Ein Leben ohne Mops ist möglich, aber sinnlos“ (Loriots Wahlspruch)
- „Dann hab ich nach zwei Jahren Jodelschule mein Jodeldiplom. Da hab ich was in der Hand. Und ich habe als Frau das Gefühl, dass ich auf eigenen Füßen stehe.“
- „Ich gehe nach den Spätnachrichten der Tagesschau ins Bett – Aber der Fernseher ist doch kaputt – Ich lasse mir von einem kaputten Fernseher nicht vorschrieben, wann ich ins Bett zu gehen habe.“ (Fernsehabend)
